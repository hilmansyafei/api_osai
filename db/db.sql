-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: osai
-- ------------------------------------------------------
-- Server version	5.5.53-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `log_id` int(11) NOT NULL,
  `action` varchar(20) DEFAULT NULL,
  `result` varchar(250) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,NULL,NULL,NULL,'2017-06-15 04:19:06'),(2,NULL,NULL,NULL,'2017-06-15 04:28:16'),(3,NULL,NULL,NULL,'2017-06-15 04:28:59'),(4,NULL,NULL,NULL,'2017-06-15 04:29:21');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cashier`
--

DROP TABLE IF EXISTS `cashier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashier` (
  `cashier_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `merchant_id` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `create_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cashier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cashier`
--

LOCK TABLES `cashier` WRITE;
/*!40000 ALTER TABLE `cashier` DISABLE KEYS */;
/*!40000 ALTER TABLE `cashier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('7qj81an3t0lj6uql9d5hu3kq3a8kaoqg','::1',1501036295,'__ci_last_regenerate|i:1501036184;id|s:2:\"19\";username|s:15:\"manager_company\";company_id|s:1:\"2\";merchant_id|N;level_user|s:1:\"2\";name|s:15:\"manager_company\";accountType|s:15:\"Manager Company\";authLogin|s:32:\"c30994dd6afc631e39d8f56433a66d83\";'),('qlgrjqv438sq5776uhkcuqg5l0btglhu','::1',1501072315,'__ci_last_regenerate|i:1501072312;'),('86lbs13ajbg940raura2kihmn2auqpd3','110.138.158.93',1501202140,'__ci_last_regenerate|i:1501202002;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('7rst313gco3lcjv2tfjtpdbroflo14nl','66.102.6.162',1501216102,'__ci_last_regenerate|i:1501216102;'),('728t30cp1s7qkmes4t0hjru2kj1nt3ou','66.102.6.162',1501216102,'__ci_last_regenerate|i:1501216102;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `city_code` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'KFC','021876522','syafeihilman@gmail.com','13860','Jakarta','2017-07-21 14:21:12','2017-07-21 14:23:18','hilman'),(2,'Mac Donals','0210987752','mcd@gmail.com','12345','jakarta','2017-07-25 16:25:27','2017-07-25 16:25:27','hilman');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `addres` varchar(255) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `total_point` int(11) DEFAULT NULL,
  `total_balance` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_customer_UNIQUE` (`customer_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_number_UNIQUE` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_priviledge`
--

DROP TABLE IF EXISTS `group_priviledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_group` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `id_submenu` int(11) DEFAULT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_user_group`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_priviledge`
--

LOCK TABLES `group_priviledge` WRITE;
/*!40000 ALTER TABLE `group_priviledge` DISABLE KEYS */;
INSERT INTO `group_priviledge` VALUES (302,1,3,7,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(303,1,3,7,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(304,1,3,7,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(305,1,4,8,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(306,1,4,8,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(307,1,4,8,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(308,1,5,9,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(309,1,5,9,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(310,1,5,9,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(311,1,5,10,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(312,1,5,10,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(313,1,5,10,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(314,1,5,12,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(315,1,5,12,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(316,1,5,12,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(317,1,2,4,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(318,1,2,4,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(319,1,2,4,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(320,1,2,5,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(321,1,2,5,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(322,1,2,5,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(323,1,2,6,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(324,1,2,6,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(325,1,2,6,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(326,1,2,11,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(327,1,2,11,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(328,1,2,11,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(329,1,2,13,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(330,1,2,13,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(331,1,2,13,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(332,1,1,1,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(333,1,1,1,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(334,1,1,1,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(335,1,1,2,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(336,1,1,2,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(337,1,1,2,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(338,7,3,7,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(339,7,3,7,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(340,7,3,7,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(341,7,4,8,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(342,7,4,8,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(343,7,4,8,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(344,7,5,9,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(345,7,5,9,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(346,7,5,9,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(347,7,5,10,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(348,7,5,10,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(349,7,5,10,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(350,7,5,12,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(351,7,5,12,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(352,7,5,12,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(353,7,7,14,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(354,7,7,14,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(355,7,7,14,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(356,7,2,4,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(357,7,2,4,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(358,7,2,4,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(359,7,2,5,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(360,7,2,5,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(361,7,2,5,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(362,7,2,6,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(363,7,2,6,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(364,7,2,6,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(365,7,2,11,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(366,7,2,11,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(367,7,2,11,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(368,7,2,13,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(369,7,2,13,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(370,7,2,13,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(371,7,1,1,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(372,7,1,1,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(373,7,1,1,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(374,7,1,3,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(375,7,1,3,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(376,7,1,3,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(377,7,1,2,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(378,7,1,2,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(379,7,1,2,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(380,9,3,7,1,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(381,9,3,7,2,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(382,9,3,7,4,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(383,9,4,8,1,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(384,9,4,8,2,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(385,9,4,8,4,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(386,9,2,11,1,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(387,9,2,11,2,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(388,9,2,11,4,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(389,9,1,1,1,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(390,9,1,1,2,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(391,9,1,1,4,'2017-07-26 02:20:27','2017-07-26 02:20:27','hilman'),(392,10,3,7,1,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(393,10,3,7,2,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(394,10,3,7,4,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(395,10,4,8,1,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(396,10,4,8,2,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(397,10,4,8,4,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(398,10,1,1,1,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(399,10,1,1,2,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(400,10,1,1,4,'2017-07-26 02:20:42','2017-07-26 02:20:42','hilman'),(401,11,4,8,1,'2017-07-26 02:20:53','2017-07-26 02:20:53','hilman'),(402,11,4,8,2,'2017-07-26 02:20:53','2017-07-26 02:20:53','hilman'),(403,11,4,8,4,'2017-07-26 02:20:53','2017-07-26 02:20:53','hilman'),(413,13,3,7,1,'2017-07-26 02:21:57','2017-07-26 02:21:57','hilman'),(414,13,4,8,1,'2017-07-26 02:21:57','2017-07-26 02:21:57','hilman'),(415,13,1,1,1,'2017-07-26 02:21:57','2017-07-26 02:21:57','hilman'),(416,12,3,7,1,'2017-07-26 02:29:23','2017-07-26 02:29:23','hilman'),(417,12,4,8,1,'2017-07-26 02:29:23','2017-07-26 02:29:23','hilman'),(418,12,2,11,1,'2017-07-26 02:29:23','2017-07-26 02:29:23','hilman');
/*!40000 ALTER TABLE `group_priviledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(255) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `cutomer_id` varchar(255) DEFAULT NULL,
  `status_invoice` varchar(45) DEFAULT NULL,
  `mercant_id` varchar(45) DEFAULT NULL,
  `cashier_id` int(11) DEFAULT NULL,
  `information` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoice_number_UNIQUE` (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_history`
--

DROP TABLE IF EXISTS `invoice_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(255) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_history`
--

LOCK TABLES `invoice_history` WRITE;
/*!40000 ALTER TABLE `invoice_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `sort_position` int(4) NOT NULL DEFAULT '0',
  `total_submenu` int(2) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `access_group` int(11) DEFAULT NULL,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Settings',6,3,'settings',4,'fa fa-cube','2017-03-29 13:28:44','2017-07-21 06:57:51','hilman'),(2,'Parameter',5,5,'parameter',4,'fa fa-cube','2017-07-19 07:10:07','2017-07-21 06:58:06','hilman'),(3,'Product Management',1,1,'product',4,'fa fa-cube','2017-07-19 07:50:29','2017-07-19 07:52:03','hilman'),(4,'Finance',2,1,'finance',4,'fa fa-cube','2017-07-19 07:51:57','2017-07-19 07:53:02','hilman'),(5,'Users Management',3,3,'usermanagement',4,'fa fa-cube','2017-07-19 08:01:27','2017-07-21 06:58:04','hilman'),(7,'Content',4,1,'content',4,'fa fa-cube','2017-07-21 06:57:20','2017-07-21 06:58:06','hilman');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merchant`
--

DROP TABLE IF EXISTS `merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mercant_id_UNIQUE` (`merchant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merchant`
--

LOCK TABLES `merchant` WRITE;
/*!40000 ALTER TABLE `merchant` DISABLE KEYS */;
INSERT INTO `merchant` VALUES (1,'0000002','KFC Jakarta Pusat',1,'jakarta pusat','021098765',NULL,'kpfjakartapusat@gmail.com','012345','2017-07-21 16:32:08','2017-07-21 16:32:08','admin_company'),(2,'0000001','Mcd Depok',2,'Depok','02123456',NULL,'mcd_depok@gmail.com','12333','2017-07-25 16:32:28','2017-07-25 16:32:28','admin_mcd');
/*!40000 ALTER TABLE `merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `content` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Osai News','Osai Nes Description                         ','Osai News Content                            ','20170726103600.jpg',1,'2017-07-26 10:28:15','2017-07-26 10:36:00','hilman');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_history`
--

DROP TABLE IF EXISTS `point_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `source` int(11) DEFAULT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_history`
--

LOCK TABLES `point_history` WRITE;
/*!40000 ALTER TABLE `point_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `point_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_no_merchant`
--

DROP TABLE IF EXISTS `seq_no_merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_no_merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_no_merchant`
--

LOCK TABLES `seq_no_merchant` WRITE;
/*!40000 ALTER TABLE `seq_no_merchant` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_no_merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_no_voucher`
--

DROP TABLE IF EXISTS `seq_no_voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_no_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_no_voucher`
--

LOCK TABLES `seq_no_voucher` WRITE;
/*!40000 ALTER TABLE `seq_no_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_no_voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_menu`
--

DROP TABLE IF EXISTS `sub_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(4) DEFAULT NULL,
  `submenu_name` varchar(100) NOT NULL,
  `sort_position` int(11) NOT NULL,
  `route_submenu` text NOT NULL,
  `content` text,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_menu`,`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_menu`
--

LOCK TABLES `sub_menu` WRITE;
/*!40000 ALTER TABLE `sub_menu` DISABLE KEYS */;
INSERT INTO `sub_menu` VALUES (1,1,'User Management',0,'userManagement','settings/userManagement',NULL,'2017-03-29 13:32:17','2017-07-19 07:52:17','hilman'),(2,1,'Group Management',1,'groupManagement','settings/groupManagement',NULL,'2017-04-10 10:37:28','2017-07-19 07:52:17','hilman'),(3,1,'Menu Management',0,'menuManagement','settings/menuManagement',NULL,'2017-04-10 10:37:28','2017-06-16 19:42:34','hilman'),(4,2,'City',0,'city','parameter/city',NULL,'2017-07-19 07:10:56','2017-07-19 07:10:56','hilman'),(5,2,'Company',0,'company','parameter/company',NULL,'2017-07-19 07:12:07','2017-07-19 07:12:07','hilman'),(6,2,'State',0,'state','parameter/state',NULL,'2017-07-19 07:12:36','2017-07-19 07:12:36','hilman'),(7,3,'Voucher',0,'voucher','product/voucher',NULL,'2017-07-19 07:50:49','2017-07-19 07:50:49','hilman'),(8,4,'Invoice History',0,'invoiceHistory','finance/invoiceHistory',NULL,'2017-07-19 07:53:02','2017-07-19 07:53:02','hilman'),(9,5,'Mercant Users',0,'mercantUser','usermanagement/mercantUser',NULL,'2017-07-19 08:02:50','2017-07-19 08:02:50','hilman'),(10,5,'Cashier',0,'cashier','usermanagement/cashier',NULL,'2017-07-21 01:56:04','2017-07-21 01:56:04','hilman'),(11,2,'Merchant',0,'merchant','parameter/merchant',NULL,'2017-07-21 01:57:34','2017-07-21 01:57:34','hilman'),(12,5,'Users',0,'users','usermanagement/users',NULL,'2017-07-21 02:16:42','2017-07-21 02:16:42','hilman'),(13,2,'Category Voucher',0,'categoryVoucher','parameter/categoryVoucher',NULL,'2017-07-21 04:07:16','2017-07-21 04:07:16','hilman'),(14,7,'News',0,'news','content/news',NULL,'2017-07-21 06:57:32','2017-07-21 06:57:32','hilman');
/*!40000 ALTER TABLE `sub_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `level_user` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `id_mercant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'Super Administrator',1,NULL,NULL,'2017-03-29 11:18:57','2017-07-21 02:06:07','hilman'),(7,'Developer',1,NULL,NULL,'2017-04-19 11:39:11','2017-04-19 11:40:07','hilman'),(9,'Administrator Company',1,NULL,NULL,'2017-07-21 02:06:29','2017-07-21 02:06:29','hilman'),(10,'Administrator Mercant',2,NULL,NULL,'2017-07-21 02:06:41','2017-07-21 02:06:41','hilman'),(11,'Cashier',3,NULL,NULL,'2017-07-21 02:11:39','2017-07-21 02:11:39','hilman'),(12,'Manager Company',2,NULL,NULL,'2017-07-21 02:11:59','2017-07-21 02:13:06','hilman'),(13,'Manager Mercant',3,NULL,NULL,'2017-07-21 02:13:16','2017-07-21 02:13:16','hilman');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_priviledge`
--

DROP TABLE IF EXISTS `user_priviledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`,`id_user`,`id_menu`,`id_submenu`,`change_by`),
  KEY `idx` (`id_user`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=1902 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_priviledge`
--

LOCK TABLES `user_priviledge` WRITE;
/*!40000 ALTER TABLE `user_priviledge` DISABLE KEYS */;
INSERT INTO `user_priviledge` VALUES (1593,1,1,1,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1594,1,1,1,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1595,1,1,1,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1596,1,1,2,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1597,1,1,2,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1598,1,1,2,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1599,1,1,3,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1600,1,1,3,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1601,1,1,3,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1602,1,2,4,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1603,1,2,4,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1604,1,2,4,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1605,1,2,5,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1606,1,2,5,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1607,1,2,5,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1608,1,2,6,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1609,1,2,6,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1610,1,2,6,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1611,1,2,11,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1612,1,2,11,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1613,1,2,11,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1614,1,2,13,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1615,1,2,13,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1616,1,2,13,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1617,1,3,7,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1618,1,3,7,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1619,1,3,7,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1620,1,4,8,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1621,1,4,8,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1622,1,4,8,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1623,1,5,9,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1624,1,5,9,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1625,1,5,9,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1626,1,5,10,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1627,1,5,10,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1628,1,5,10,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1629,1,5,12,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1630,1,5,12,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1631,1,5,12,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1632,1,7,14,1,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1633,1,7,14,2,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1634,1,7,14,4,'2017-07-26 02:22:31','2017-07-26 02:22:31','hilman'),(1671,9,1,1,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1672,9,1,1,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1673,9,1,1,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1674,9,1,2,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1675,9,1,2,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1676,9,1,2,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1677,9,1,3,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1678,9,1,3,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1679,9,1,3,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1680,9,2,4,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1681,9,2,4,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1682,9,2,4,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1683,9,2,5,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1684,9,2,5,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1685,9,2,5,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1686,9,2,6,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1687,9,2,6,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1688,9,2,6,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1689,9,2,11,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1690,9,2,11,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1691,9,2,11,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1692,9,2,13,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1693,9,2,13,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1694,9,2,13,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1695,9,3,7,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1696,9,3,7,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1697,9,3,7,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1698,9,4,8,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1699,9,4,8,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1700,9,4,8,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1701,9,5,9,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1702,9,5,9,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1703,9,5,9,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1704,9,5,10,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1705,9,5,10,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1706,9,5,10,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1707,9,5,12,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1708,9,5,12,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1709,9,5,12,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1710,9,7,14,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1711,9,7,14,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1712,9,7,14,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1755,10,1,1,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1756,10,1,1,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1757,10,1,1,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1758,10,2,11,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1759,10,2,11,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1760,10,2,11,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1761,10,3,7,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1762,10,3,7,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1763,10,3,7,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1764,10,4,8,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1765,10,4,8,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1766,10,4,8,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1776,11,1,1,1,'2017-07-26 02:23:09','2017-07-26 02:23:09','hilman'),(1777,11,3,7,1,'2017-07-26 02:23:09','2017-07-26 02:23:09','hilman'),(1778,11,4,8,1,'2017-07-26 02:23:09','2017-07-26 02:23:09','hilman'),(1791,12,1,1,1,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1792,12,1,1,2,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1793,12,1,1,4,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1794,12,3,7,1,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1795,12,3,7,2,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1796,12,3,7,4,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1797,12,4,8,1,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1798,12,4,8,2,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1799,12,4,8,4,'2017-07-26 02:23:23','2017-07-26 02:23:23','hilman'),(1842,16,1,1,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1843,16,1,1,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1844,16,1,1,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1845,16,2,11,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1846,16,2,11,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1847,16,2,11,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1848,16,3,7,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1849,16,3,7,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1850,16,3,7,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1851,16,4,8,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1852,16,4,8,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1853,16,4,8,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1866,17,1,1,1,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1867,17,1,1,2,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1868,17,1,1,4,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1869,17,3,7,1,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1870,17,3,7,2,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1871,17,3,7,4,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1872,17,4,8,1,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1873,17,4,8,2,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1874,17,4,8,4,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1884,18,4,8,1,'2017-07-26 02:24:06','2017-07-26 02:24:06','hilman'),(1885,18,4,8,2,'2017-07-26 02:24:06','2017-07-26 02:24:06','hilman'),(1886,18,4,8,4,'2017-07-26 02:24:06','2017-07-26 02:24:06','hilman'),(1899,19,2,11,1,'2017-07-26 02:29:43','2017-07-26 02:29:43','hilman'),(1900,19,3,7,1,'2017-07-26 02:29:43','2017-07-26 02:29:43','hilman'),(1901,19,4,8,1,'2017-07-26 02:29:43','2017-07-26 02:29:43','hilman');
/*!40000 ALTER TABLE `user_priviledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_group` int(11) NOT NULL DEFAULT '0',
  `foto` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `level_user` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `google_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `phone_number` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NULL DEFAULT NULL,
  `change_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'hilman','hilman',NULL,'6fd4829c2ae25048d24bde25a6759a985bf2b1d44d7c0e5ce7dceb324cba7d93eb6c364319b9c3530775e799b619f360730e23b8756103cc276b78a4bc6751c5',7,'20170311084801.png',1,1,0,NULL,NULL,'','',NULL,NULL,NULL,'2017-03-30 07:50:13','2017-07-26 02:22:31','hilman'),(9,'Fitrah Ramadhan','fitrah',NULL,'458d1d516f82c568062a1e54fb8f8d2c7a6de121faefa497d04a03b4e0ac7abf42522d58a6bfbe8e64d0fa0a601f608515f87e45865f0f00f1540161d024a125',7,NULL,1,1,0,NULL,NULL,'','',NULL,NULL,NULL,'2017-06-15 06:58:18','2017-07-26 02:22:42','hilman'),(10,'Admin Company','admin_company',NULL,'cd3189125c8e820eb699f3d62634358bd8d7d2594c98ab6e3119da973cd907094c1e30da6299d52f2fb41b257a7fec8ec5b042bffa56bc7928ce7dc6d72fe3d1',9,NULL,1,2,2,NULL,NULL,'','',NULL,NULL,NULL,'2017-07-21 02:19:26','2017-07-26 02:22:54','hilman'),(11,'Hilman','manager_merchant',NULL,'9f9c6439af5e2290621f4e5c045316c7d1543ce48b2ba2a25bb8243170f788836f28548f1b6f8e556db96137076bcd8f9fb69befbcb2e9c8ad03b60d7aef2b72',13,NULL,1,3,2,NULL,1,'','',NULL,NULL,NULL,'2017-07-21 09:44:38','2017-07-26 02:23:09','hilman'),(12,'Admin Merchant','admin_merchant',NULL,'1a621bc5533aa078221097d1026a04b3ac71405c2679e3c8654ad81520b53618590a97013867c699f269d5647d572703bff2705d16b46c73b5c6df2ff04bc826',10,NULL,1,3,2,NULL,1,'','',NULL,NULL,NULL,'2017-07-25 09:07:52','2017-07-26 02:23:23','hilman'),(15,'Fitrah','cashier',NULL,'8f03fff7607a53697e9a7bc54c6156f3498b8ccacce7e9b7774332002d531d8fd90f3e7990be629945cac2ade6e0b1353eaa118a64243c73640a8137433f3ac7',11,NULL,1,3,1,NULL,1,'','',NULL,NULL,NULL,'2017-07-25 09:20:42','2017-07-25 09:24:27','hilman'),(16,'Admin MCD','admin_mcd',NULL,'1b19e385ad8a3ceca068c4134f09ab2dbba78eba8e00e072a2e0b2f7b3d2d16509b8286b9fa7f748da607f510f112f7842241aa2dcaa15a1c863e213b31bf1c9',9,NULL,1,2,2,NULL,NULL,'','',NULL,NULL,NULL,'2017-07-25 09:25:52','2017-07-26 02:23:43','hilman'),(17,'Admin','admin_merchant_mcd',NULL,'1a621bc5533aa078221097d1026a04b3ac71405c2679e3c8654ad81520b53618590a97013867c699f269d5647d572703bff2705d16b46c73b5c6df2ff04bc826',10,NULL,1,3,2,NULL,2,'','',NULL,NULL,NULL,'2017-07-25 09:32:54','2017-07-26 02:23:54','hilman'),(18,'Yunan','cashier_mcd',NULL,'8f03fff7607a53697e9a7bc54c6156f3498b8ccacce7e9b7774332002d531d8fd90f3e7990be629945cac2ade6e0b1353eaa118a64243c73640a8137433f3ac7',11,NULL,1,3,2,NULL,2,'','',NULL,NULL,NULL,'2017-07-25 09:37:34','2017-07-26 02:24:06','hilman'),(19,'manager_company','manager_company',NULL,'f30d922c53d631d6d7207ee40af92ce30bdca1785748c8405802be03cf8b4ce7da9847977f8a153186a630a60efcb1b5a990c2521ecd48ebd7ec904425d929b9',12,NULL,1,2,2,NULL,NULL,'','',NULL,NULL,NULL,'2017-07-26 02:26:21','2017-07-26 02:29:43','hilman'),(20,'','hilman_test',NULL,'$2y$10$kkxFG1mSU/T4YLciBp7ug.yHFZDdVtvhI/K1StBTPpTEtKUsmv11W',0,NULL,0,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL),(27,'Hilman Syafei','syafeihilman2@gmail.com','hilmans','$2y$10$MAVUdss5wWGnSo7.1yOule2f6T.L7gz9ijCcw1Fqm9RFecGHsShtC',0,'$2y$10$yMGnwZ7xsd.RVeZ56V13keMnkCQ.glX8ESg8WlraB.0mYoyrT3CbW',1,4,NULL,'depok',NULL,'12345','123456','02112345678',NULL,'1111',NULL,NULL,NULL),(28,'Hilman Syafei','hilmanandro@gmail.com','hilman_syafei','$2y$10$49TLnMryq19wzPSC7d/TGOMT1/W6qkBt5fud/1Xl0SKnBnAUkHKJe',0,NULL,1,4,NULL,'jakarta',NULL,'12345','123456','02112345678',NULL,NULL,'2017-07-26 17:46:13','2017-07-26 17:46:13',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voucher`
--

DROP TABLE IF EXISTS `voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` varchar(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `bonus_point` int(11) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `voucher_category` int(11) DEFAULT NULL,
  `term_condition` text,
  `information` text,
  `image` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voucher_id_UNIQUE` (`voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voucher`
--

LOCK TABLES `voucher` WRITE;
/*!40000 ALTER TABLE `voucher` DISABLE KEYS */;
INSERT INTO `voucher` VALUES (4,'0000008',NULL,200000,0,'Ayam Gepuk','2017-07-07',1,0,1,1,'Term and condition','Information','20170721020455.jpg','2017-07-21 13:38:55','2017-07-28 07:35:40','hilman');
/*!40000 ALTER TABLE `voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voucher_category`
--

DROP TABLE IF EXISTS `voucher_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voucher_category`
--

LOCK TABLES `voucher_category` WRITE;
/*!40000 ALTER TABLE `voucher_category` DISABLE KEYS */;
INSERT INTO `voucher_category` VALUES (1,'Food','2017-07-21 11:18:25','2017-07-21 11:18:25','hilman'),(2,'Hotel','2017-07-21 11:18:32','2017-07-21 11:18:32','hilman');
/*!40000 ALTER TABLE `voucher_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_settings`
--

DROP TABLE IF EXISTS `web_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `content` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_settings`
--

LOCK TABLES `web_settings` WRITE;
/*!40000 ALTER TABLE `web_settings` DISABLE KEYS */;
INSERT INTO `web_settings` VALUES (1,'faq','-','pertanyaan Faq',NULL,NULL,NULL),(2,'about_us','-','text about us',NULL,NULL,NULL);
/*!40000 ALTER TABLE `web_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-28 14:45:29
