<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['jwt.auth']], function () {
	//ROUTE USER
    Route::post('/profile', 'ApiController@profile');
    Route::post('/update_fcm', 'ApiController@updateFcm');
    Route::post('/faq', 'ApiController@faq');
	Route::post('/about_us', 'ApiController@aboutUs');
	Route::post('/term_condition', 'ApiController@termCondition');
	Route::post('/contact_us', 'ApiController@contactUs');
	Route::post('/change_password', 'ApiController@changePassword');
	Route::post('/change_profile_picture', 'ApiController@changeProfilePicture');
	Route::post('/change_profile_data', 'ApiController@changeProfileData');
	Route::post('/insert_comment', 'ApiController@insertComment');
	Route::post('/search_user', 'ApiController@searchUser');
	Route::get('/point_history', 'ApiController@pointHistory');
	Route::get('/my_voucher_active', 'ApiController@myVoucherActive');
	Route::get('/my_voucher_history', 'ApiController@myVoucherHistory');
	Route::post('/send_point', 'ApiController@sendPoint');
	Route::post('/detail_my_voucher', 'ApiController@detailMyVoucherActive');
	Route::get('/logout', 'ApiController@logoutUser');
	Route::post('/comment_news', 'ApiController@commentNews');
	Route::get('/deposit', 'ApiController@deposit');
	//ROUTE USER

	//ROUTE CASHIER
	Route::post('/change_password_cashier', 'CashierController@changePasswordCashier');
	Route::post('/verify_voucher', 'CashierController@verifyVoucher');
	Route::post('/redeem_voucher', 'CashierController@redeemVoucher');
	Route::post('/confirmation_user', 'CashierController@confirmationUser');
	//ROUTE CASHIER

	//ROUTE MERCHANT
	Route::post('/change_password_merchant', 'MerchantController@changePasswordMerchant');
	Route::get('/profile_merchant', 'MerchantController@profileMerchant');
	Route::post('/list_last_activity', 'MerchantController@listLastActivity');
	Route::post('/get_redeem', 'MerchantController@getRedeem');
	Route::post('/get_list_redeem', 'MerchantController@getlistRedeem');
	Route::get('/get_list_withdraw', 'MerchantController@getListWithdraw');
	Route::post('/request_withdraw', 'MerchantController@requestWithdraw');
	//ROUTE MERCHANT

	Route::get('/save_transaction', 'SnapController@saveTransaction');
	Route::post('/transaction_by_point', 'PaymentController@tansactionByPoint');
	Route::post('/list_bank_account', 'ApiController@listBankAccount');
	Route::post('/payment_check', 'SnapController@paymentCheck');
});
Route::get('/send_email', 'ApiController@sendEmail');	

//OSAI API
//API USER
Route::get('/auth_app/{username}/{password}', 'AuthenticateController@authApp');
Route::get('/hash_pass/{password}', 'AuthenticateController@hashPass');
Route::post('/get_version', 'ApiController@getVersion');
Route::post('/authenticate', 'AuthenticateController@authenticate');
Route::post('/register', 'AuthenticateController@register');
Route::post('/authenticate_facebook', 'AuthenticateController@authenticateFacebook');
Route::post('/authenticate_google', 'AuthenticateController@authenticateGoogle');
Route::get('/slider', 'ApiController@slider');
Route::post('/top_voucher', 'ApiController@topVoucher');
Route::post('/detail_voucher', 'ApiController@detailVoucher');
Route::post('/voucher_by_category', 'ApiController@voucherByCategory');
Route::post('/category_voucher', 'ApiController@categoryVoucher');
Route::post('/news', 'ApiController@news');
Route::post('/detail_news', 'ApiController@detailNews');
Route::get('/gen_qr', 'ApiController@genQr');
Route::post('/popular', 'ApiController@popularList');
Route::post('/most_buy', 'ApiController@mostBuy');
Route::post('/top_search', 'ApiController@topSearch');
Route::get('/finish_transaction', 'SnapController@finishTransaction');
Route::get('/unfinish', 'SnapController@unfinish');
Route::get('/notification_test', 'SnapController@notificationTest');
Route::get('/payment_method', 'ApiController@paymentMethod');
Route::post('/change_notif_stat', 'ApiController@changeStatusNotif');
Route::post('/voucher_point', 'ApiController@voucherPoint');

Route::post('/notification', 'SnapController@notification');

Route::post('/get_status', 'SnapController@getStatus');
Route::get('/send_notif', 'ApiController@sendNotif');
//END API USER

//API CASHIER
Route::post('/authenticate_cashier', 'AuthenticateController@authenticateCashier');
//END API CASHIER

//END API MERCHANT
Route::post('/authenticate_merchant', 'AuthenticateController@authenticateMerchant');
//END OSAI API

//END API OSAI

//PAYMENT GATEWAY
Route::get('/vtweb', 'PagesController@vtweb');
Route::get('/vtdirect', 'PagesController@vtdirect');
Route::post('/vtdirect', 'PagesController@checkout_process');
Route::get('/vt_transaction', 'PagesController@transaction');
Route::post('/vt_transaction', 'PagesController@transaction_process');
Route::post('/vt_notif', 'PagesController@notification');
Route::get('/snap', 'SnapController@snap');
Route::get('/snaptoken', 'SnapController@token');
Route::post('/snapfinish', 'SnapController@finish');
//END PAYMENT GATEWAY

