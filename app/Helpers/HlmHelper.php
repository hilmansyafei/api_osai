<?php

namespace App\Helper;

use DB;
use QRcode;

class HlmHelper
{
	
	function __construct()
	{
		# code...
	}

	public static function genQr(){
        $PNG_TEMP_DIR = $_SERVER['DOCUMENT_ROOT']."/api_osai/public/qr/";
     
        $check = true;
        while ($check) {
        	$res = HlmHelper::genCode();
        	$checkAvailable = DB::table('invoice_history')->where('qr_code',$res)->first();
        	if (count($checkAvailable)==0) {
        		$check=false;
        	}
        }
        $filename = $PNG_TEMP_DIR.$res.'.png';
        QRcode::png($res, $filename, "M", "20", 2);  
        
        return $res;
    }

    public static function genCode(){
        $chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $res = "";
        $check = true;
        while ($check) {
        	for ($i = 0; $i < 10; $i++) {
	         $res .= $chars[mt_rand(0, strlen($chars)-1)];
	        }
	        $checkAvailable = DB::table('invoice_history')->where('confirmation_number',$res)->first();
        	if (count($checkAvailable)==0) {
        		$check=false;
        	}
        }
        	
        return $res;
    }

	public static function genOrderId($idUser="")
	{
		DB::table('seq_order_id')->insert(array('id_user'=>$idUser));
		$getSeqNumber = DB::table('seq_order_id')->where('id_user',$idUser)->first();
		$resID = $getSeqNumber->id;
		$lenId = strlen($resID);
		for ($i=0; $i < (7-$lenId) ; $i++) { 
			$resID = "0".$resID;
		}

		$resID = "ORD-".$resID;

		DB::table('seq_order_id')->where('id_user',$idUser)->delete();
		return $resID;
	}

	public static function genInvoiceNumber($idUser="")
	{
		DB::table('seq_invoice_number')->insert(array('id_user'=>$idUser));
		$getSeqNumber = DB::table('seq_invoice_number')->where('id_user',$idUser)->first();
		$resID = $getSeqNumber->id;
		$lenId = strlen($resID);
		for ($i=0; $i < (7-$lenId) ; $i++) { 
			$resID = "0".$resID;
		}

		$resID = "INV-".$resID;

		DB::table('seq_invoice_number')->where('id_user',$idUser)->delete();

		return $resID;
	}

	public static function insertData($tableName="",$dataInsert=array(),$id = false){
		if ($tableName=="users") {
			$dataInsert['created_at'] = date("Y-m-d H:i:s");
			$dataInsert['updated_at'] = date("Y-m-d H:i:s");
		}else{
			$dataInsert['create_date'] = date("Y-m-d H:i:s");
			$dataInsert['change_date'] = date("Y-m-d H:i:s");
			$dataInsert['change_by'] = Session('username');
		}
		
		if ($id) {
			return DB::table($tableName)->insertGetId($dataInsert);
		}else{
			return DB::table($tableName)->insert($dataInsert);
		}
	}

	public static function selectData($fields="",$tableName="",$where="",$order="",$group="",$limit=""){
		if ($where!="") {
			$where = " WHERE ".$where;
		}
		if ($order!="") {
			$order = " ORDER BY ".$order;
		}
		if ($group!="") {
			$group = " GROUP BY ".$group;
		}
		if ($limit!="") {
			$limit = " LIMIT ".$limit;
		}
		
		$sql = "SELECT $fields FROM $tableName $where $group $order $limit";
		return DB::select(DB::raw($sql));
	}

	public static function updateData($tableName="",$data=array(),$where=""){
		if ($where!="") {
			$where = " WHERE ".$where;
		}else{
			echo "You cannot update data without where condition !!!";die();
		}
		if ($tableName=="users") {
			$data['updated_at'] = date("Y-m-d H:i:s");
		}else{
			$data['change_date'] = date("Y-m-d H:i:s");
			$data['change_by'] = Session('username');
		}
		$updateFields = " SET ";
		$count = 1;
		foreach ($data as $key => $value) {
			$cekMin = count(explode("-", $value));
			if ($count==count($data)) {
				if (strripos($value,"+") || $cekMin==2) {
					$updateFields .= "$key = $value";
				}else{
					$updateFields .= "$key = '$value'";
				}
			}else{
				if (strripos($value,"+") || $cekMin==2) {
					$updateFields .= "$key = $value,";
				}else{
					$updateFields .= "$key = '$value',";
				}
				$count++;
			}
		}
		$sql = "UPDATE $tableName $updateFields $where";
		return DB::statement($sql);
	}

	public static function deleteData($tableName="",$where=""){
		if ($where!="") {
			$where = " WHERE ".$where;
		}else{
			echo "You cannot update data without where condition !!!";die();
		}
	
		$sql = "DELETE FROM $tableName $where";
		return DB::statement($sql);
	}

	public static function genVerNumber(){
        $digits = 6;
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    public static function sendNotif($fcm_token="",$body="",$title="Verification Number Transaction"){
        // API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAA5fB-N5g:APA91bFIgR0tgOdK0kQm-7Bhwm8zT4JG0js0kYy2OSZAiVacqw7MGgPw-VaX0p3EAFKnrH4K1uyflTMm8_eFVHhdrnHJk5Uokqfwk0-SeipSEEvkD7TkuNJdbRiwwqA9s7qrHzz9CBIe' );
        // prep the bundle
        $msg = array
        (
            'body'  => $body,
            'title'     => $title,
            'vibrate'   => 1,
            'sound'     => "default"
        );

        $fields = array
        (
            'to'  => $fcm_token,
            'notification'=> $msg
        );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }
}

?>