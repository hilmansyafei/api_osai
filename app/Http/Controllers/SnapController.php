<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Http\Controllers\Controller;
use DB;
use JWTAuth;
use App\Veritrans\Midtrans;
use App\Veritrans\Veritrans;
use App\Helper\HlmHelper;
use QRcode;

class SnapController extends Controller
{
    public function __construct()
    {   
        Midtrans::$serverKey = 'VT-server-PlH8wf3EGayGQWvNLixchX_e';
        //set is production to true for production mode
        Midtrans::$isProduction = false;
    }

    public function snap()
    {
        return view('snap_checkout');
    }

    public static function genQr(){

        $PNG_TEMP_DIR = $_SERVER['DOCUMENT_ROOT']."/api_osai/public/qr/";
        $res =  SnapController::genCode();
        $filename = $PNG_TEMP_DIR.$res.'.png';
        QRcode::png($res, $filename, "M", "20", 2);    

        return $res;
    }

    public static function genCode()
    {
        $chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 10; $i++) {
         $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        return $res;
    }

    public function paymentCheck(Request $request){
        $getReq = $request->only('voucher_id','total_order');
        $getVoucher = DB::table('voucher')->where('voucher_id',$getReq['voucher_id'])->first();
        if (count($getVoucher)==0) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>"Voucher Item not found",
                'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        if ($getVoucher->stock==0 || $getVoucher->stock<$getReq['total_order']) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>"Stock not enough",
                'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        $res = array(
            'responeCode'=>0,
            'responeMessage'=>"Direct to payment page",
            'payment_page_link'=>env('APP_DOMAIN')."public/index.php/save_transaction",
            'status'=>"Success"
        );
        return response()->json($res,200);


    }   

    public function saveTransaction(Request $request) 
    {
        $midtrans = new Midtrans;
        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('voucher_id','total_order');
        if (!isset($_GET['voucher_id']) && !isset($_GET['total_order'])) {
            echo "set voucher_id sama total_order cuk";
            die();
        }
        $getReq['voucher_id'] = $_GET['voucher_id'];
        $getReq['total_order'] = $_GET['total_order'];


        // if (empty($getReq)) {
        //     $getReq['voucher_id']  = 'VCR-0000006';
        //     $getReq['total_order'] = 3;
        // }
        
        $getVoucher = DB::table('voucher')->where('voucher_id',$getReq['voucher_id'])->first();
        $order_id = HlmHelper::genOrderId($user['id']);
        if (count($getVoucher)==0) {
            $res = array(
            	'responeCode'=>1,
            	'responeMessage'=>"Voucher Item not found",
                'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        if ($getVoucher->stock==0 || $getVoucher->stock<$getReq['total_order']) {
            $res = array(
            	'responeCode'=>1,
                'responeMessage'=>"Stock not enough",
                'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        $saveToOrder = array(
        	'voucher_id'=> $getReq['voucher_id'],
			'company_id' =>$getVoucher->company_id,
			'merchant_id'=>$getVoucher->merchant_id,
			'customer_id'=>$user['id'],
			'order_id'=>$order_id,
			'order_date'=>date('Y-m-d H:i:s'),
			'total'=>$getReq['total_order'],
			'total_price'=>$getReq['total_order']*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
			'create_date'=>date("Y-m-d H:i:s"),
			'change_date'=>date("Y-m-d H:i:s"),
			'change_by'=>$user['id']
        );
        if ($getVoucher->payment_type==4) {
            $saveToOrder['point'] = $getVoucher->point;
        }
        DB::table('tbl_order')->insert($saveToOrder);


        //$saveToPaymentTransaction = array('order_id'=>$order_id);
        //DB::table('payment_transaction')->insert($saveToPaymentTransaction);

        $transaction_details = array(
			'order_id'      => $order_id,
			'gross_amount'=>$getReq['total_order']*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100)
		);

        // Populate items
        $items = [
            array(
                'id'        => $getVoucher->voucher_id,
                'price'     => $getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
                'quantity'  => $getReq['total_order'],
                'name'      => $getVoucher->name
            )
        ];

        // Populate customer's billing address
        $billing_address = array(
            'first_name'    => $user['name'],
            'last_name'     => $user['last_name'],
            'address'       => $user['address'],
            'city'          => $user['city'],
            'postal_code'   => $user['postal_code'],
            'phone'         => $user['phone_number'],
            'country_code'  => 'IDN'
        );

        // Populate customer's Info
        $customer_details = array(
            'first_name'      => $user['name'],
            'last_name'       => $user['last_name'],
            'email'           => $user['email'],
            'phone'           => $user['phone_number']
        );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            echo '<html>
                      <head>
                      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
                      <script>
                        $( document ).ready(function() {
                            $("#pay-button").hide();
                            $("#pay-button").click();
                        });
                      </script>
                    <script type="text/javascript"
                                src="https://app.sandbox.midtrans.com/snap/snap.js"
                                data-client-key="VT-client-q3ohGoloqFoviGMW"></script> 
                    <style>
                    .app-container{
                        width : 100% !important;
                    }
                    .button-main{
                        width : auto !important;
                    }
                    </style>
                      </head>
                      <body>
                        <button id="pay-button">Pay!</button>
                        <script type="text/javascript">
                          var payButton = document.getElementById(\'pay-button\');
                          payButton.addEventListener(\'click\', function () {
                            snap.pay(\''.$snap_token.'\'); // store your snap token here
                          });
                        </script>
                      </body>
                    </html>';
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function token() 
    {
        error_log('masuk ke snap token dri ajax');
        $midtrans = new Midtrans;

        $transaction_details = array(
            'order_id'      => uniqid(),
            'gross_amount'  => 200000
        );

        // Populate items
        $items = [
            array(
                'id'        => 'item3',
                'price'     => 100000,
                'quantity'  => 1,
                'name'      => 'Adidas f50'
            ),
            array(
                'id'        => 'item3',
                'price'     => 50000,
                'quantity'  => 2,
                'name'      => 'Nike N90'
            )
        ];

        // Populate customer's billing address
        $billing_address = array(
            'first_name'    => "Andri",
            'last_name'     => "Setiawan",
            'address'       => "Karet Belakang 15A, Setiabudi.",
            'city'          => "Jakarta",
            'postal_code'   => "51161",
            'phone'         => "081322311801",
            'country_code'  => 'IDN'
        );

        // // Populate customer's shipping address
        // $shipping_address = array(
        //     'first_name'    => "John",
        //     'last_name'     => "Watson",
        //     'address'       => "Bakerstreet 221B.",
        //     'city'          => "Jakarta",
        //     'postal_code'   => "51162",
        //     'phone'         => "081322311801",
        //     'country_code'  => 'IDN'
        //     );

        // Populate customer's Info
        $customer_details = array(
            'first_name'      => "Hilman",
            'last_name'       => "Syafei",
            'email'           => "hilman@asdasd.com",
            'phone'           => "081322311801"
            //'billing_address' => $billing_address
            //'shipping_address'=> $shipping_address
        );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
            //echo $snap_token;
            echo '<html>
                      <head>
                      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
                      <script>
                        $( document ).ready(function() {
                            $("#pay-button").hide();
                            $("#pay-button").click();
                        });
                      </script>
                    <script type="text/javascript"
                                src="https://app.sandbox.midtrans.com/snap/snap.js"
                                data-client-key="VT-client-q3ohGoloqFoviGMW"></script> 
                    <style>
                    .app-container{
                        width : 100% !important;
                    }
                    .button-main{
                        width : auto !important;
                    }
                    </style>
                      </head>
                      <body>
                        <button id="pay-button">Pay!</button>
                        <script type="text/javascript">
                          var payButton = document.getElementById(\'pay-button\');
                          payButton.addEventListener(\'click\', function () {
                            snap.pay(\''.$snap_token.'\'); // store your snap token here
                          });
                        </script>
                      </body>
                    </html>';
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function finish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        echo $result->status_message . '<br>';
        echo 'RESULT <br><pre>';
        var_dump($result);
        echo '</pre>' ;
    }

    public function unfinish(Request $request)
    {
        echo "Mohon Maaf Transaksi Gagal Dilakukan";
    }

    public static function processVoucher($result)
    {
    	if (isset($result['permata_va_number'])) {
            $result['va_number'] = $result['permata_va_number'];
            $result['bank'] = 'permata';
            unset($result['permata_va_number']);
        }
        if (isset($result['va_numbers'])) {
            $result['bank'] = $result['va_numbers'][0]['bank'];
            $result['va_number'] = $result['va_numbers'][0]['va_number'];
            unset($result['va_numbers']);
            unset($result['payment_amounts']);
        }
        
        $checkdata = DB::table('payment_transaction')
        				->where('order_id',$result['order_id'])
        				->first();
        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($result);
            $checkdata = DB::table('payment_transaction')
            				->where('order_id',$result['order_id'])
            				->first();
        }else{
            DB::table('payment_transaction')
            	->where('order_id',$result['order_id'])
            	->update($result);    
        }
        
        $getOrder = DB::table('tbl_order')->where('order_id',$result['order_id'])->first();
        $getVoucher = DB::table('voucher')->where('voucher_id',$getOrder->voucher_id)->first();
        $checkInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();

        $total_order  = $getOrder->total;
        $voucher_add = 0;
        if ($getVoucher->count_buy!=0) {
            $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
            $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
        }
        if (count($checkInvoice)==0) {
            $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
            $saveToInvoice = array(
            	'voucher_id'=>$getVoucher->voucher_id,
				'invoice_number'=>$invoice_number,
				'company_id' =>$getVoucher->company_id,
				'merchant_id'=>$getVoucher->merchant_id,
				'customer_id'=>$getOrder->customer_id,
				'order_id'=>$getOrder->order_id,
				'order_date'=>date('Y-m-d H:i:s'),
				'total'=>$total_order,
				'bonus_voucher'=>$getVoucher->get_buy,
				'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
				'payment_type'=>$getVoucher->payment_type,
				'create_date'=>date("Y-m-d H:i:s"),
				'change_date'=>date("Y-m-d H:i:s"),
				'change_by'=>$getOrder->change_by
			);
            if ($getVoucher->payment_type==4) {
                $saveToInvoice['total_price_point']=$getOrder->total*$getVoucher->point;
            }else{
                $saveToInvoice['total_price_point'] = 0;
            }
            DB::table('invoice')->insert($saveToInvoice);

            for ($i=0; $i <$getOrder->total; $i++) { 
                $saveToInvoiceHistory = array(
                	'order_id' =>$getOrder->order_id,
					'name'=>$getVoucher->name,
					'invoice_number'=>$invoice_number,
					'voucher_id'=>$getVoucher->voucher_id,
					'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
					'bonus_point'=>$getVoucher->bonus_point,
					'payment_type'=>$getVoucher->payment_type,
					'voucher_category'=>$getVoucher->voucher_category,
					'expired_date'=>$getVoucher->expired_date,
					'create_date'=>date('Y-m-d H:i:s'),
					'change_date'=>date('Y-m-d H:i:s'),
					'change_by'=>$getOrder->change_by
				);
                if ($getVoucher->payment_type==4) {
                    $saveToInvoiceHistory['point'] = $getVoucher->point;
                }
                DB::table('invoice_history')->insert($saveToInvoiceHistory);
            }

            for ($i=0; $i < $voucher_add; $i++) { 
                $saveToInvoiceHistory = array(
                	'order_id' =>$getOrder->order_id,
					'name'=>$getVoucher->name,
					'invoice_number'=>$invoice_number,
					'voucher_id'=>$getVoucher->voucher_id,
					'price'=>0,
					'point'=>0,
					'bonus_point'=>$getVoucher->bonus_point,
					'payment_type'=>$getVoucher->payment_type,
					'voucher_category'=>$getVoucher->voucher_category,
					'expired_date'=>$getVoucher->expired_date,
					'voucher_type'=>1,
					'create_date'=>date('Y-m-d H:i:s'),
					'change_date'=>date('Y-m-d H:i:s'),
					'change_by'=>$getOrder->change_by
				);
                DB::table('invoice_history')->insert($saveToInvoiceHistory);
            }
        }

        $getInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();

        $transaction = $result['transaction_status'];
        $type = $result['payment_type'];
        $order_id = $result['order_id'];
        if (isset($result['fraud_status'])) {
            $fraud = $result['fraud_status'];
        }
        
        $isProcess = false;
        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          	if ($type == 'credit_card'){
	            if($fraud == 'challenge'){
	            	// TODO set payment status in merchant's database to 'Challenge by FDS'
	            	// TODO merchant should decide whether this transaction is authorized or not in MAP
	            }else {
	             	// TODO set payment status in merchant's database to 'Success'
                    DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));
                    $isProcess = true;
	            }
        	}
        }else if ($transaction == 'settlement'){
        	// TODO set payment status in merchant's database to 'Settlement'
        	if ($type!="credit_card") {
        		DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));
                $isProcess = true;
        	}
        }else if($transaction == 'pending'){
          	// TODO set payment status in merchant's database to 'Pending'
        }else if ($transaction == 'deny') {
          	// TODO set payment status in merchant's database to 'Denied'
          	DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>2));
        }else if ($transaction == 'cancel'){
        	//TODO cancel for permata VA
        	DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>2));
        }else if($transaction == 'expire' || $transaction == 'Expired'){
        	//TODO expire for permata VA
        	DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>2));
        }

        if ($isProcess) {
            //Payment with point and money
            //cut point
            if ($getVoucher->payment_type==4) {
                $point_history_out   = array( 
                  'type' =>4,
                  'point'=>($getVoucher->point*$getOrder->total),
                  'customer_id'=>$getOrder->customer_id,
                  'mutation'=>2,
                  'invoice_number'=>$getInvoice->invoice_number,
                  'create_date'=>date('Y-m-d H:i:s'),
                  'change_date'=>date('Y-m-d H:i:s'),
                  'change_by'=>$getOrder->change_by
                );
                DB::table('point_history')->insert($point_history_out);
                DB::table('users')
                    ->where('id',$getOrder->customer_id)
                    ->decrement('point',($getVoucher->point*$getOrder->total));
            }
            //end cut point

	        $dataInvoiceHistory = DB::table('invoice_history')
	                    			->where('order_id',$getInvoice->order_id)
	                    			->get();
	        if ($dataInvoiceHistory[0]->qr_code=="" && $getVoucher->voucher_category!=1) {
	        	//pengurangan stock
	        	$arrUpdate = array(
	            	'stock'=>($getVoucher->stock-$getInvoice->total),
	                'sold'=>($getVoucher->sold+$getInvoice->total)
	            );
	            DB::table('voucher')
	            	->where('voucher_id',$getInvoice->voucher_id)
	            	->update($arrUpdate);
	        	//end pengurangan stock
	            	
	            foreach ($dataInvoiceHistory as $key => $value) {
	                $qrCode = SnapController::genQr();
	                $updateQr = array('qr_code' =>$qrCode);
	                DB::table('invoice_history')
	                	->where('id',$value->id)
	                	->update($updateQr);

	                $point_history = array(
	                	'type' =>1,
						'point'=>$value->bonus_point,
						'mutation'=>1,
						'customer_id'=>$getInvoice->customer_id,
						'invoice_number'=>$getInvoice->invoice_number,
                        'create_date'=>date('Y-m-d H:i:s'),
                        'change_date'=>date('Y-m-d H:i:s'),
                        'change_by'=>$getOrder->change_by
					);

	                if($value->bonus_point>0 && $value->voucher_type!=1){
	                    DB::table('point_history')->insert($point_history);  
	                    DB::table('users')
	                    	->where('id',$getInvoice->customer_id)
	                    	->increment('point',$value->bonus_point);  
	                }
	                
	                
	            }
	        }elseif ($getVoucher->voucher_category==1) {
	            foreach ($dataInvoiceHistory as $key => $value) {
	                $updateUsed = array('used' =>1);
	                DB::table('invoice_history')
	                	->where('id',$value->id)
	                	->update($updateUsed);

	                $point_history = array(
	                	'type' =>3,
						'point'=>$getVoucher->point_get,
						'mutation'=>1,
						'customer_id'=>$getInvoice->customer_id,
						'invoice_number'=>$getInvoice->invoice_number,
                        'create_date'=>date('Y-m-d H:i:s'),
                        'change_date'=>date('Y-m-d H:i:s'),
                        'change_by'=>$getOrder->change_by
					);
	                
	                DB::table('point_history')->insert($point_history);    
	                
	                DB::table('users')
	                	->where('id',$getInvoice->customer_id)
	                	->increment('point',$getVoucher->point_get);
	            }
	        }
	    }

    }

    public function notification()
    {
    	$midtrans = new Midtrans;
        $json_result = file_get_contents('php://input');
        $insertLogNotif = array('notif_data' =>$json_result);
        HlmHelper::insertData('log_notif_payment',$insertLogNotif);
        $result = json_decode($json_result,true);

        SnapController::processVoucher($result);
    }

    public function notificationTest($value='')
    {
    	$midtrans = new Midtrans;
        //echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $json_result = '{
                          "transaction_time": "2018-03-21 11:16:53",
                          "transaction_status": "capture",
                          "transaction_id": "866fb89b-78a5-4431-817d-d410207a1f69",
                          "status_message": "midtrans payment notification",
                          "status_code": "200",
                          "signature_key": "728b85ba0b9cd0682ed8fded5257dde1d415da70aeb83dac9a38ec9cfd4ce16908b6772b1c9edc3d8b33b9eece2deab4d85ab91ed3dee0ea27e0fa26159b5dd3",
                          "payment_type": "credit_card",
                          "order_id": "ORD-0000530",
                          "masked_card": "481111-1114",
                          "gross_amount": "1000000.00",
                          "fraud_status": "accept",
                          "eci": "05",
                          "channel_response_message": "Approved",
                          "channel_response_code": "00",
                          "bank": "mandiri",
                          "approval_code": "1521605814145"
                        }';
		$insertLogNotif = array('notif_data' =>$json_result);
        $result = json_decode($json_result,true);
        SnapController::processVoucher($result);
    }

    public function getStatus(Request $request)
    {
        $allData = $request->only('order_id');
        echo $allData['order_id'];die();
        $order = DB::table('payment_transaction')->where('order_id',$allData['order_id'])->first();
        if (count($order)==1) {
            $duration_since_order = time() - $order->transaction_time;
            //check If the order after 360 seconds (6 minutes), and still no pending/capture/settlement/deny/cancel/failure status.
            if ( (time()-strtotime($order->transaction_time)) <= 6  && $order->transaction_status === null ){
                //Do get status for that order
                return '{
                          "status_code": "500",
                          "status_message": "Request stills under 6 minutes, manual check can do after 6 minutes request does not update"
                        }';
            }else{
                $objResult = Midtrans::status($order->order_id);
                $result = json_encode($objResult);
                $result = json_decode($result,TRUE);

                if (isset($result['permata_va_number'])) {
		            $result['va_number'] = $result['permata_va_number'];
		            $result['bank'] = 'permata';
		            unset($result['permata_va_number']);
		        }
		        if (isset($result['va_numbers'])) {
		            $result['bank'] = $result['va_numbers'][0]['bank'];
		            $result['va_number'] = $result['va_numbers'][0]['va_number'];
		            unset($result['va_numbers']);
		            unset($result['payment_amounts']);
		        }
                

                $checkdata = DB::table('payment_transaction')
        				->where('order_id',$result['order_id'])
        				->first();
                
                SnapController::processVoucher($result);

            }
            
        }elseif (count($order)>1) {
            return '{
                          "status_code": "500",
                          "status_message": "Double order id"
                        }';
        }else{
            return '{
                      "status_code": "404",
                      "status_message": "The requested resource is not found"
                    }';
        }
        
        /**
         * Assume we can load order data from database to $orders array
         * Each $order is an object, with property of:
         * $order->timestamp : unix timestamp when order is created (timestamp)
         * $order->order_id  : order_id that is sent to Midtrans API (string)
         * $order->transaction_status : status of the order (string)
         * $order->set_status($status): update order status according to parameter (string)
         * $order->save()    : save the order to database
         */
    }

    public function finishTransaction(Request $request)
    {
        $getReq = $request->only('order_id','status_code');
        $checkdata = DB::table('payment_transaction')
        				->where('order_id',$getReq['order_id'])
        				->first();

        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($getReq);
        }else{
            DB::table('payment_transaction')
            	->where('order_id',$getReq['order_id'])
            	->update($getReq);    
        }

        $getOrder = DB::table('tbl_order')
        				->where('order_id',$getReq['order_id'])
        				->first();

        $getVoucher = DB::table('voucher')
        				->where('voucher_id',$getOrder->voucher_id)
        				->first();
        
        if (count($getVoucher)==0) {
            $res = array(
            	'responeCode'=>1,
            	'responeMessage'=>"Voucher Iten not found",
            	'status'=>"Failed"
            );
            return response()->json($res,404);
        }
        $checkInvoice = DB::table('invoice')
        					->where('order_id',$getReq['order_id'])
        					->first();

        $total_order  = $getOrder->total;
        $voucher_add = 0;
        if ($getVoucher->count_buy!=0) {
            $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
            $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
        }
        if (count($checkInvoice)==0) {
            $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
            $saveToInvoice = array(
            	'voucher_id'=>$getVoucher->voucher_id,
				'invoice_number'=>$invoice_number,
				'company_id' =>$getVoucher->company_id,
				'merchant_id'=>$getVoucher->merchant_id,
				'customer_id'=>$getOrder->customer_id,
				'order_id'=>$getOrder->order_id,
				'order_date'=>date('Y-m-d H:i:s'),
				'total'=>$total_order,
				'bonus_voucher'=>$getVoucher->get_buy,
				'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
				'payment_type'=>$getVoucher->payment_type,
				'create_date'=>date("Y-m-d H:i:s"),
				'change_date'=>date("Y-m-d H:i:s"),
				'change_by'=>$getOrder->change_by
			);
            if ($getVoucher->payment_type==4) {
                $saveToInvoice['total_price_point']=$getOrder->total*$getVoucher->point;
            }else{
                $saveToInvoice['total_price_point'] = 0;
            }
            DB::table('invoice')->insert($saveToInvoice);

            for ($i=0; $i <$getOrder->total; $i++) { 
                $saveToInvoiceHistory = array(
                	'order_id' =>$getOrder->order_id,
					'name'=>$getVoucher->name,
					'invoice_number'=>$invoice_number,
					'voucher_id'=>$getVoucher->voucher_id,
					'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
					'bonus_point'=>$getVoucher->bonus_point,
					'payment_type'=>$getVoucher->payment_type,
					'voucher_category'=>$getVoucher->voucher_category,
					'expired_date'=>$getVoucher->expired_date,
					'create_date'=>date('Y-m-d H:i:s'),
					'change_date'=>date('Y-m-d H:i:s'),
					'change_by'=>$getOrder->change_by
				);
                if ($getVoucher->payment_type==4) {
                    $saveToInvoiceHistory['point']=$getVoucher->point;
                }else{
                    $saveToInvoiceHistory['point']=0;
                }
                DB::table('invoice_history')->insert($saveToInvoiceHistory);
            }

            if ($getOrder->total_order>=$getVoucher->count_buy && $getVoucher->count_buy!=0 && $getVoucher->get_buy!=0) {
                for ($i=0; $i < $voucher_add; $i++) { 
                    $saveToInvoiceHistory = array(
						'order_id' =>$getOrder->order_id,
						'name'=>$getVoucher->name,
						'invoice_number'=>$invoice_number,
						'voucher_id'=>$getVoucher->voucher_id,
						'price'=>0,
						'point'=>0,
						'bonus_point'=>$getVoucher->bonus_point,
						'payment_type'=>$getVoucher->payment_type,
						'voucher_category'=>$getVoucher->voucher_category,
						'expired_date'=>$getVoucher->expired_date,
						'voucher_type'=>1,
						'create_date'=>date('Y-m-d H:i:s'),
						'change_date'=>date('Y-m-d H:i:s'),
						'change_by'=>$getOrder->change_by
					);
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }
            }
        }


        echo "Selamat Transaksi Telah Selesai";
    }
}    