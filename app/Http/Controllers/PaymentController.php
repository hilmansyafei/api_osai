<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use App\Http\Requests;
use Auth;
use Hash;
use QRcode;
use App\Helper\HlmHelper;
use App\Mail\MailOsai;
use Mail;

class PaymentController extends Controller
{
    public function tansactionByPoint(Request $request)
    {   
        $getReq = $request->only('voucher_id','total_order');
        $getVoucher = DB::table('voucher')->where('voucher_id',$getReq['voucher_id'])->first();
        $user = JWTAuth::parseToken()->authenticate();

        if (count($getVoucher)==0) {
            $res = array(
              'responeCode'=>1,
              'responeMessage'=>"Voucher Item not found",
              'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        if ($getVoucher->payment_type!=2 && $getVoucher->payment_type!=3) {
            $res = array(
              'responeCode'=>1,
              'responeMessage'=>"Payment type not valid",
              'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        $getDataUSer = DB::table('users')->where('id',$user['id'])->first();
        if ($getVoucher->point>$getDataUSer->point) {
            $res = array(
              'responeCode'=>1,
              'responeMessage'=>"Point tidak cukup",
              'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        if ($getVoucher->stock==0 || $getVoucher->stock<$getReq['total_order']) {
            $res = array(
              'responeCode'=>1,
              'responeMessage'=>"Stock not enough",
              'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        $total_order  = $getReq['total_order'];
        $voucher_add = 0;
        if ($getVoucher->count_buy!=0) {
            $total_order =$getReq['total_order']+floor($getReq['total_order']/$getVoucher->count_buy)*$getVoucher->get_buy;
            $voucher_add = floor($getReq['total_order']/$getVoucher->count_buy)*$getVoucher->get_buy;;
        }

        
        $order_id = HlmHelper::genOrderId($user['id']);
        $saveToOrder = array(
          'voucher_id'=> $getReq['voucher_id'],
          'company_id' =>$getVoucher->company_id,
          'merchant_id'=>$getVoucher->merchant_id,
          'customer_id'=>$user['id'],
          'order_id'=>$order_id,
          'order_date'=>date('Y-m-d H:i:s'),
          'total'=>$total_order,
          'total_price'=>0,
          'create_date'=>date("Y-m-d H:i:s"),
          'change_date'=>date("Y-m-d H:i:s"),
          'change_by'=>$user['id']
        );
        DB::table('tbl_order')->insert($saveToOrder);

        $arrUpdate = array('stock'=>($getVoucher->stock-$saveToOrder['total']),
                           'sold'=>($getVoucher->sold+$saveToOrder['total']));
        DB::table('voucher')
          ->where('voucher_id',$getVoucher->voucher_id)
          ->update($arrUpdate);


        $getOrder = DB::table('tbl_order')
                      ->where('order_id',$order_id)
                      ->first();
        $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);

        $saveToInvoice = array(
          'voucher_id'=>$getVoucher->voucher_id,
          'invoice_number'=>$invoice_number,
          'company_id' =>$getVoucher->company_id,
          'merchant_id'=>$getVoucher->merchant_id,
          'customer_id'=>$getOrder->customer_id,
          'order_id'=>$getOrder->order_id,
          'order_date'=>date('Y-m-d H:i:s'),
          'total'=>$total_order,
          'total_price_point'=>$getVoucher->point*$getReq['total_order'],
          'bonus_voucher'=>$getVoucher->get_buy,
          'total_price'=>0,
          'payment_type'=>3,
          'status_invoice'=>3,
          'create_date'=>date("Y-m-d H:i:s"),
          'change_date'=>date("Y-m-d H:i:s"),
          'change_by'=>$getOrder->change_by
        );
        DB::table('invoice')->insert($saveToInvoice);

        $point_history_out   = array( 
          'type' =>4,
          'point'=>($getVoucher->point*$getOrder->total),
          'customer_id'=>$user['id'],
          'mutation'=>2,
          'invoice_number'=>$invoice_number,
          'create_date'=>date('Y-m-d H:i:s'),
          'change_date'=>date('Y-m-d H:i:s'),
          'change_by'=>$getOrder->change_by
        );
        DB::table('point_history')->insert($point_history_out);

        for ($i=0; $i < $getOrder->total - $voucher_add; $i++) { 
            $qrCode = HlmHelper::genQr();

            $saveToInvoiceHistory = array(
              'order_id' =>$getOrder->order_id,
              'name'=>$getVoucher->name,
              'invoice_number'=>$invoice_number,
              'voucher_id'=>$getVoucher->voucher_id,
              'price'=>$getVoucher->price * (100-$getVoucher->discount/100) * ((100-$getVoucher->discount_plus)/100),
              'point'=>$getVoucher->point,
              'bonus_point'=>$getVoucher->bonus_point,
              'payment_type'=>3,
              'voucher_category'=>$getVoucher->voucher_category,
              'expired_date'=>$getVoucher->expired_date,
              'qr_code'=>$qrCode,
              'create_date'=>date('Y-m-d H:i:s'),
              'change_date'=>date('Y-m-d H:i:s'),
              'change_by'=>$getOrder->change_by
            );
            DB::table('invoice_history')->insert($saveToInvoiceHistory);
            
            if($getVoucher->bonus_point>0){      
              $point_history = array(
                'type' =>1,
                'point'=>$getVoucher->bonus_point,
                'mutation'=>1,
                'customer_id'=>$getOrder->customer_id,
                'invoice_number'=>$invoice_number,
                'create_date'=>date("Y-m-d H:i:s"),
                'change_date'=>date("Y-m-d H:i:s")
              );
              DB::table('point_history')->insert($point_history); 
              DB::table('users')
                    ->where('id',$getOrder->customer_id)
                    ->increment('point',$getVoucher->bonus_point);
            }


        }

        for ($i=0; $i < $voucher_add; $i++) { 
            $qrCode = HlmHelper::genQr();
            $saveToInvoiceHistory = array(
              'order_id' =>$getOrder->order_id,
              'name'=>$getVoucher->name,
              'invoice_number'=>$invoice_number,
              'voucher_id'=>$getVoucher->voucher_id,
              'price'=>0,
              'point'=>0,
              'bonus_point'=>$getVoucher->bonus_point,
              'payment_type'=>3,
              'voucher_category'=>$getVoucher->voucher_category,
              'expired_date'=>$getVoucher->expired_date,
              'qr_code'=>$qrCode,
              'voucher_type'=>1,
              'create_date'=>date('Y-m-d H:i:s'),
              'change_date'=>date('Y-m-d H:i:s'),
              'change_by'=>$getOrder->change_by
            );
            DB::table('invoice_history')->insert($saveToInvoiceHistory);
        }
        DB::table('users')
                    ->where('id',$user['id'])
                    ->decrement('point',($getVoucher->point*$getOrder->total));

        $res = array(
          'responeCode'=>0,
          'responeMessage'=>"Transaksi Berhasil",
          'status'=>"Success"
        );
        return response()->json($res);
    }
}
