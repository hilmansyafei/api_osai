<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use App\Http\Requests;
use Auth;
use Hash;
use QRcode;
use App\Helper\HlmHelper;
use App\Mail\MailOsai;
use Mail;

class ApiController extends Controller
{
	public function __construct() {
   		//$this->middleware('jwt.auth', ['except' => ['authenticate']]);
   		//$this->middleware('jwt.refresh');
   		//$newToken = JWTAuth::parseToken()->refresh();
	}

	//osai API USER
    public function sendEmail()
    {
        //Mail::to('syafeihilman@gmail.com')->send(new MailOsai);
        HlmHelper::sendNotif("e09-2UVEKVQ:APA91bGUrUwMJ_gBBfz-81XP9bWszAsRpBaQPplWwAmL61MsbqAlhZCMHXDaPVTbyVZiL2fU2442pAbDavFI2hAtzL_FBQSgD0By24EA-o_1w8PGHO0SPO-jUAv2KBsOwuK8GH2Ugeeh",HlmHelper::genVerNumber(),$title="Test Notif");
    }


	public function getVersion()
	{
		$ret = array(
            'version'=>'1',
            'release'=>'2017-07-26 06:00:21',
            'responeCode'=>0,
            'responeMessage'=>"Load Success",
            'status'=>"Success"
        );

        // $getAllUser = DB::table('users')->get();
        // foreach ($getAllUser as $key => $value) {
        //     $dataUpadate = array('digital_user_id'=>'7'.date("ymdHis").$value->id);

        //     DB::table('users')->where('id',$value->id)->update($dataUpadate);
        // }
		return response()->json($ret)->setEncodingOptions(JSON_NUMERIC_CHECK);
	}

	public function profile(Request $request) 
    {
		$user = JWTAuth::parseToken()->authenticate();
        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','users.merchant_id','=','merchant.id')
                        ->select('users.*', 'company.name as company_name','merchant.name as merchant_name')
                        ->where('users.id',$user['id'])->first();
        unset($profile->password);
        unset($profile->company_id);
        unset($profile->merchant_id);
        $res = array(
            'profile'=>$profile,
            'responeCode'=>0,
            'responeMessage'=>'Load Success',
            'status'=>'Success'
        );
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function updateFcm(Request $request)
    {
        $getReq = $request->only('fcm');
        $user = JWTAuth::parseToken()->authenticate();
        $getUser = DB::table('users')->where('id',$user['id'])->update(array('fcm_token' =>$getReq['fcm']));
        $code = 200;
        if (count($getUser)==0) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>'User not Found',
                'status'=>'Failed'
            );
            $code = 404;
        }else{
            $res = array(
                'responeCode'=>0,
                'responeMessage'=>"Load Success",
                'status'=>"Success"
            );
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);        
    }

    public function faq(Request $request)
    {
        $getData = DB::table('web_settings')->where('name','faq')->first();
        $code = 200;
        if (count($getData)==0) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>'Data not Found',
                'status'=>'Failed'
            );
            $code = 404;
        }else{
            $res = array(
                'faq'=>$getData->content,
                'responeCode'=>0,
                'responeMessage'=>"Load Success",
                'status'=>"Success"
            );
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function aboutUs(Request $request)
    {
        $getData = DB::table('web_settings')->where('name','about_us')->first();
        $code = 200;
        if (count($getData)==0) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>'Data Not Found',
                'status'=>'Error'
            );
            $code = 404;
        }else{
            $res = array(
                'about_us'=>$getData->content,
                'responeCode'=>0,
                'responeMessage'=>"Load Success",
                'status'=>"Success"
            );   
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function termCondition(Request $request)
    {
        $getData = DB::table('web_settings')->where('name','term_condition')->first();
        $code = 200;
        if (count($getData)==0) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>'Data Not Found',
                'status'=>'Error'
            );
            $code = 404;
        }else{
            $res = array(
                'term_condition'=>$getData->content,
                'responeCode'=>0,
                'responeMessage'=>'Load Success',
                'status'=>'Success'
            );    
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function contactUs(Request $request)
    {	
    	$getReq = $request->only('email','subject','message');
    	//send Email
        HlmHelper::insertData('contact_us',$getReq);
        
        $res = array(
            'responeCode'=>0,
            'responeMessage'=>'Send Success',
            'status'=>'Success'
        );
    	return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);  
    }

    public function changePassword(Request $request)
    {
    	$getReq = $request->only('old_password','new_password');
    	$user = JWTAuth::parseToken()->authenticate();
    	$credentials = array(
            'username'=>$user['username'],
            'password'=>$getReq['old_password']
        );
        $code = 200;
    	if (!Auth::attempt($credentials)) {
    		$res = array(
                'responeCode'=>1,
                'responeMessage'=>'Old password is invalid',
                'status'=>'failed'
            );
    		$code = 404;
    	}else{
            DB::table('users')
                ->where('user_group',0)
                ->where('id',$user['id'])
                ->update(array('password'=>Hash::make($getReq['new_password'])));
            $res = array(
                'responeCode'=>0,
                'responeMessage'=>'Change password success',
                'status'=>"Success"
            );
        }
    	
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function changeProfilePicture(Request $request)
    {
    	$getReq = $request->only('foto');
    	$user = JWTAuth::parseToken()->authenticate();
        $code = 200;
    	if (!$user) {
    		$res = array(
                'responeCode'=>1,
                'responeMessage'=>'Invalid Token',
                'status'=>'failed'
            );
    		$code = 404;
    	}else{
            $file = $request->file('foto');
            $fileName = date('YmdHisu').$user['id'].".".$file->getClientOriginalExtension();
            $request->file('foto')->move("profile/", $fileName);
            $getOldData = DB::table('users')->where('id',$user['id'])->first();

            if (file_exists(public_path().'/profile/'.$getOldData->foto)) {
                unlink(public_path().'/profile/'.$getOldData->foto);
            }
            
            DB::table('users')
                ->where('id',$user['id'])
                ->update(array('foto'=>$fileName));

            $res = array(
                'responeCode'=>0,
                'responeMessage'=>'Change photo profile success',
                'status'=>'Success'
            );
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function changeProfileData(Request $request)
    {
    	$getReq = $request->only('email','name','phone_number','address');
    	$user = JWTAuth::parseToken()->authenticate();
        $code = 200;

    	$checkEmail = DB::table('users')
                        ->where('email',$getReq['email'])
                        ->where('id','!=',$user['id'])
                        ->first();

    	if ($checkEmail) {
    		$res = array(
                'responeCode'=>1,
                'responeMessage'=>'Email has been used',
                'status'=>"failed"
            );
    		$code = 404;
    	}else{
            DB::table('users')
                ->where('id',$user['id'])
                ->update($getReq);

            $res = array(
                'responeCode'=>0,
                'responeMessage'=>'Change data profile success',
                'status'=>'Success'
            );
        }
    	return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function slider(Request $request)
    {
        $getData = DB::table('voucher')
                    ->where('stat_slider',1)
        			->join('merchant','merchant.id','=','voucher.merchant_id')
        			->join('company','company.company_id','=','merchant.company_id')
        			->select('voucher.*','merchant.name as merchant_name','company.name as company_name','company.company_id')
        			->get();

        $code = 200;
        unset($getData[0]->merchant_id);
        $link = "http://188.166.190.210/osai/data_uploads/voucher/".$getData[0]->company_id."/";
        foreach ($getData as $key => $value) {
        	$value->image = $link.$value->image;
        }
        if (count($getData)==0) {
            $res = array(
                'responeCode'=>1,
                'responeMessage'=>'Data Not Found',
                'status'=>"Error"
            );
            $code = 404;
        }else{
            $res = array(
                'slider'=>$getData,
                'responeCode'=>0,
                'responeMessage'=>'Load Success',
                'status'=>"Success"
            );
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function topVoucher(Request $request)
    {
        $getReq = $request->only('sort_by','sort_def','filter_by','filter_val','filter_between','filter_between_val');

        $custom_where = " where 1 =1 ";
        if ($request->has('filter_by') && $request->has('filter_val')) {
            $custom_where .= " and voucher.".$getReq['filter_by']." in (".$getReq['filter_val'].") ";
        }
        $custom_order = "";
        if($request->has('sort_by') && $request->has('sort_def')){
            $custom_order = " order by ".$getReq['sort_by']." ".$getReq['sort_def'];
        }

        if($request->has('filter_between') && $request->has('filter_between_val')){
            $expBetween = explode(",", $getReq['filter_between_val']);
            $custom_where .= " and ".$getReq['filter_between']." between ".$expBetween[0]." and ".$expBetween[1];
        }

        $code = 200;
        $getData = DB::select(DB::raw("select voucher.*,merchant.name as merchant_name,company.name as company_name,company.company_id 
                from voucher 
                    join merchant 
                        on merchant.id = voucher.merchant_id
                    join company 
                        on company.company_id = merchant.company_id
                    $custom_where $custom_order
                "));
        unset($getData[0]->merchant_id);
        foreach ($getData as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
        }
        if (count($getData)==0) {
            $res = array(
                'topVoucher'=>$getData,
                'responeCode'=>1,
                'responeMessage'=>'Data Not Found',
                'status'=>'Error'
            );
            $code = 404;
        }else{
            $res = array(
                'topVoucher'=>$getData,
                'responeCode'=>0,
                'responeMessage'=>'Load Success',
                'status'=>'Success'
            );
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function detailVoucher(Request $request)
    {
    	$getReq = $request->only('voucher_id');
        $getData = DB::table('voucher')
        			->join('merchant','merchant.id','=','voucher.merchant_id')
        			->join('company','company.company_id','=','merchant.company_id')
        			->select('voucher.*','merchant.name as merchant_name','company.name as company_name','company.company_id','merchant.latit','merchant.longit','merchant.image as image_merchant','merchant.phone_number as merchant_phone_number')
        			->where('voucher.id',$getReq['voucher_id'])
        			->first();
        $code = 200;
        if (empty($getData)) {
        	$res = array(
                'detailVoucher'=>$getData,
                'responeCode'=>1,
                'responeMessage'=>'Data not Found',
                'status'=>'Failed'
            );
    		$code = 404;
        }else{
            unset($getData->merchant_id);
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$getData->company_id."/";
            $getData->image = $link.$getData->image;

            $linkMerchant = "http://188.166.190.210/osai/data_uploads/merchant/";
            $getData->image_merchant = $linkMerchant.$getData->image_merchant;

            if (count($getData)==0) {
                $res = array(
                    'responeCode'=>1,
                    'responeMessage'=>'Data Not Found',
                    'status'=>'Error'
                );
                return response()->json($res, 404);
            }
            DB::table('voucher')->where('id',$getReq['voucher_id'])->increment('count_view');

            //insert statistic data
            if(JWTAuth::parseToken()){
                $user = JWTAuth::parseToken()->authenticate();
                $checkStat = HlmHelper::selectData('*','statistic_data','user_id = '.$user['id'].' and date="'.date('Y-m-d').'" and voucher_id = '.$getReq['voucher_id']);
                if (count($checkStat)==0) {
                    $statInsert = array(
                        'date' => date('Y-m-d'),
                        'user_id' => $user['id'],
                        'voucher_id' => $getReq['voucher_id'],
                        'total_view' => 1
                    );
                    HlmHelper::insertData('statistic_data',$statInsert);
                }else{
                    DB::table('statistic_data')
                        ->where('user_id',$user['id'])
                        ->where('voucher_id',$getReq['voucher_id'])
                        ->where('date',date('Y-m-d'))
                        ->increment('total_view');
                }
            }
            //end insert data statistic

            $res = array(
                'detailVoucher'=>$getData,
                'responeCode'=>0,
                'responeMessage'=>'Load Success',
                'status'=>'Success'
            );    
        }
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function voucherByCategory(Request $request)
    {
    	$getReq = $request->only('category_id','sort_by','sort_def','filter_by','filter_val','filter_between','filter_between_val');

        $custom_where = " where voucher.voucher_category = ".$getReq['category_id']." and expired_date > '".date('Y-m-d H:i:s')."' ";
        
        if ($request->has('filter_by') && $request->has('filter_val')) {
            $custom_where .= " and voucher.".$getReq['filter_by']." like '%".$getReq['filter_val']."%'";
        }
        $custom_order = "";
        if($request->has('sort_by') && $request->has('sort_def')){
            $custom_order = " order by ".$getReq['sort_by']." ".$getReq['sort_def'];
        }

        if($request->has('filter_between') && $request->has('filter_between_val')){
            $expBetween = explode(",", $getReq['filter_between_val']);
            $custom_where .= " and ".$getReq['filter_between']." between ".$expBetween[0]." and ".$expBetween[1];
        }
        
        $getData = DB::select(DB::raw("select voucher.*,merchant.name as merchant_name,company.name as company_name,company.company_id 
                from voucher 
                    join merchant 
                        on merchant.id = voucher.merchant_id
                    join company 
                        on company.company_id = merchant.company_id
                    $custom_where $custom_order
                "));
        unset($getData[0]->merchant_id);
        foreach ($getData as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
        }

        $code = 200;
        if (count($getData)==0) {
            $res = array("voucher"=>$getData,
                         'responeCode'=>1,
                         'responeMessage'=>"Data Not Found",
                         'status'=>"Error");
            $code = 404;
        }else{
            $res = array("voucher"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");        
        }
        	
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
    
    public function categoryVoucher(Request $request)
    {
        $getReq = $request->only('sort_by','sort_def');
        $getData = DB::table('voucher_category')
                    ->select(DB::raw('(select count(*) from voucher where voucher_category = voucher_category.id and expired_date >= now()) as total, voucher_category.id,voucher_category.name,voucher_category.image'))
                    ->orderBy('id','asc')
                    ->groupBy('voucher_category.id', 'voucher_category.image','voucher_category.name')
                    ->get();

        $link = "http://188.166.190.210/osai/data_uploads/voucher_category/";
        foreach ($getData as $key => $value) {
            $value->image = $link.$value->image;
        }
        $code = 200;
        if (count($getData)==0) {
        	$res = array("categoryVoucher"=>NULL,
                         'responeCode'=>1,
                         'responeMessage'=>"Data not Found",
                         'status'=>"Failed");
    		$code = 404;
        }else{
            $res = array("categoryVoucher"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function news(Request $request)
    {
        $getData = DB::select("SELECT n.*,
                                  (SELECT count(*)
                                   FROM comment_news
                                   WHERE n.id = news_id) AS total_comment
                                FROM news AS n ");
        $code = 200;
        if (count($getData)==0) {	
        	$res = array("news"=>NULL,
                         'responeCode'=>1,
                         'responeMessage'=>"Data not Found",
                         'status'=>"Failed");
    		$code = 404;
        }else{
            foreach ($getData as $key => $value) {
                $link = "http://188.166.190.210/osai/data_uploads/news/".$value->image;
                $value->image = $link;
            }
            
            $res = array("news"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function detailNews(Request $request)
    {
    	$getReq = $request->only('blog_id');
        $getData = DB::table('news')
        				->where('id',$getReq['blog_id'])
        				->first();
        $code = 200;
        if (count($getData)==0) {	
        	$res = array("detailNews"=>NULL,
                         'responeCode'=>1,
                         'responeMessage'=>"Data not Found",
                         'status'=>"Failed");
    		$code = 404;
        }else{
            $link = "http://188.166.190.210/osai/data_uploads/news/";
        
            $getData->image = $link.$getData->image;

            $res = array("detailNews"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
        
    	return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function commentNews(Request $request)
    {
        $getReq = $request->only('blog_id');
        $getData = DB::table('comment_news')
                        ->select('comment_news.*','users.name')
                        ->join('users','comment_news.user_id','=','users.id')
                        ->where('news_id',$getReq['blog_id'])
                        ->get();
        $code = 200;
        if (count($getData)==0) {   
            $res = array("commentNews"=>NULL,
                         'responeCode'=>1,
                         'responeMessage'=>"Data not Found",
                         'status'=>"Failed");
            $code = 404;
        }else{
            $res = array("commentNews"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function insertComment(Request $request)
    {
    
        $getReq = $request->only('blog_id','comment');
        $user = JWTAuth::parseToken()->authenticate();
        $insertData = array('news_id'=>$getReq['blog_id'],
                            'comment'=>$getReq['comment'],
                            'user_id'=>$user['id']);

        DB::table('comment_news')->insert($insertData);
        $res = array('responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function popularList(Request $request)
    {
        $getReq = $request->only('sort_by','sort_def','filter_by','filter_val','filter_between','filter_between_val');

        $custom_where = " where 1 =1 and expired_date > '".date('Y-m-d H:i:s')."' ";
        if ($request->has('filter_by') && $request->has('filter_val')) {
            $custom_where .= " and voucher.".$getReq['filter_by']." like '%".$getReq['filter_val']."%'";
        }
        $custom_order = " order by voucher.sold desc ";
        if($request->has('sort_by') && $request->has('sort_def')){
            $custom_order = " order by ".$getReq['sort_by']." ".$getReq['sort_def'];
        }

        if($request->has('filter_between') && $request->has('filter_between_val')){
            $expBetween = explode(",", $getReq['filter_between_val']);
            $custom_where .= " and ".$getReq['filter_between']." between ".$expBetween[0]." and ".$expBetween[1];
        }

        $getData = DB::select(DB::raw("select voucher.*,merchant.name as merchant_name,company.name as company_name,company.company_id 
                from voucher 
                    join merchant 
                        on merchant.id = voucher.merchant_id
                    join company 
                        on company.company_id = merchant.company_id
                    $custom_where $custom_order
                "));
        unset($getData[0]->merchant_id);
        foreach ($getData as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
        }
        $code = 200;
        if (count($getData)==0) {
            $res = array("voucher"=>$getData,
                         'responeCode'=>1,
                         'responeMessage'=>"Data Not Found",
                         'status'=>"Error");
            $code = 404;
        }else{
            $res = array("voucher"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function mostBuy(Request $request)
    {

        $getReq = $request->only('sort_by','sort_def','filter_by','filter_val','filter_between','filter_between_val');

        $custom_where = " where 1 =1 and expired_date > '".date('Y-m-d H:i:s')."' ";
        if ($request->has('filter_by') && $request->has('filter_val')) {
            $custom_where .= " and voucher.".$getReq['filter_by']." like '%".$getReq['filter_val']."%'";
        }
        $custom_order = " order by voucher.sold desc";
        if($request->has('sort_by') && $request->has('sort_def')){
            $custom_order = " order by ".$getReq['sort_by']." ".$getReq['sort_def'];
        }

        if($request->has('filter_between') && $request->has('filter_between_val')){
            $expBetween = explode(",", $getReq['filter_between_val']);
            $custom_where .= " and ".$getReq['filter_between']." between ".$expBetween[0]." and ".$expBetween[1];
        }

        $getData = DB::select(DB::raw("select voucher.*,merchant.name as merchant_name,company.name as company_name,company.company_id 
                from voucher 
                    join merchant 
                        on merchant.id = voucher.merchant_id
                    join company 
                        on company.company_id = merchant.company_id
                    $custom_where $custom_order
                "));
        unset($getData[0]->merchant_id);
        foreach ($getData as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
        }
        $code = 200;
        if (count($getData)==0) {
            $res = array("voucher"=>$getData,
                         'responeCode'=>1,
                         'responeMessage'=>"Data Not Found",
                         'status'=>"Error");
            $code = 404;
        }else{
            $res = array("voucher"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function topSearch(Request $request)
    {
        $getReq = $request->only('sort_by','sort_def','filter_by','filter_val','filter_between','filter_between_val');

        $custom_where = " where 1 =1 and expired_date > '".date('Y-m-d H:i:s')."' ";
        if ($request->has('filter_by') && $request->has('filter_val')) {
            $custom_where .= " and voucher.".$getReq['filter_by']." like '%".$getReq['filter_val']."%'";
        }
        $custom_order = " order by voucher.sold desc";
        if($request->has('sort_by') && $request->has('sort_def')){
            $custom_order = " order by ".$getReq['sort_by']." ".$getReq['sort_def'];
        }

        if($request->has('filter_between') && $request->has('filter_between_val')){
            $expBetween = explode(",", $getReq['filter_between_val']);
            $custom_where .= " and ".$getReq['filter_between']." between ".$expBetween[0]." and ".$expBetween[1];
        }
        
        $getData = DB::select(DB::raw("select voucher.*,merchant.name as merchant_name,company.name as company_name,company.company_id 
                from voucher 
                    join merchant 
                        on merchant.id = voucher.merchant_id
                    join company 
                        on company.company_id = merchant.company_id
                    $custom_where $custom_order
                "));
        unset($getData[0]->merchant_id);
        foreach ($getData as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
        }
        $code = 200;
        if (count($getData)==0) {
            $res = array("voucher"=>$getData,
                         'responeCode'=>1,
                         'responeMessage'=>"Data Not Found",
                         'status'=>"Error");
            $code = 404;
        }else{
            $res = array("voucher"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function deposit(Request $request) 
    {
        $user = JWTAuth::parseToken()->authenticate();
        $deposit = DB::table('users')
                        ->select('saldo','point')
                        ->where('id',$user['id'])
                        ->first();

        $res = array("deposit"=>$deposit,
                     'responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function paymentMethod(Request $request) 
    {
        $paymentMethod = DB::table('tbl_ref')
                        ->where('group_ref',1)->get();
        $res = array("paymentMethod"=>$paymentMethod,
                     'responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function changeStatusNotif(Request $request) 
    {
        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('stat');
        $updateStatus = array('stat_notif'=>$getReq['stat']);
        DB::table('users')
            ->where('id',$user['id'])
            ->update($updateStatus);

        $res = array('responeCode'=>0,
                     'responeMessage'=>"Chang Notification Status Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function searchUser(Request $request) 
    {
        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('search_param');
        $users = DB::table('users')
            ->where(function ($query) use ($getReq)
            {
                $query->where('name','like',"%".$getReq['search_param']."%")
                      ->orWhere('phone_number','like',"%".$getReq['search_param']."%")
                      ->orWhere('email','like',"%".$getReq['search_param']."%");
            })
            ->where('user_group',0)
            ->where('id','<>',$user['id'])
            ->get();
        $res = array("users"=>$users,
                     'responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function pointHistory(Request $request) 
    {
        $user = JWTAuth::parseToken()->authenticate();
        $point_history = DB::table('point_history')
                ->join('tbl_ref', function ($join) {
                    $join->on('point_history.mutation', '=', 'tbl_ref.id_ref')
                         ->where('tbl_ref.group_ref', '=', 2);
                    })
                ->leftJoin('users','point_history.sent_by','=','users.id')
                ->select('point_history.id',DB::raw('(if(type=1,"Bonus Point",if(type=3,"Voucher Point",if(type=4,"Payment With Point",if(type=5,concat("Received Point by ",users.name),concat("Send Point To ",users.name)))))) as source'),'point_history.point','invoice_number','mutation')
                ->orderBy('point_history.create_date','desc')
                ->where('customer_id',$user['id'])
                ->get();

                //->select('point_history.id',DB::raw('(if(mutation=1 and sent_by is null,"Received Point by Bonus",if(mutation=1 and sent_by is not null,concat("Received Point by ",users.name),if(mutation=3,"Voucher Point",if(mutation=4,"Payment With Point",concat("Send Point To ",users.name)))))) as source'),'point_history.point','invoice_number','mutation')
        $res = array("pointHistory"=>$point_history,
                     'responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");

        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function myVoucherActive(Request $request) 
    {
        $user = JWTAuth::parseToken()->authenticate();
        $myVoucher = DB::select('SELECT v.*,
                                       ih.used,
                                       ih.id AS id_history,
                                       i.status_invoice,
                                       IF(i.status_invoice=3,3, If(ih.used=1,4, If(ih.expired_date<Now(),5, If(i.status_invoice=0,2, If(pt.status_code=201,2,1))))) AS status
                                FROM invoice_history AS ih
                                LEFT JOIN voucher AS v ON ih.`voucher_id` = v.`voucher_id`
                                LEFT JOIN `payment_transaction` AS pt ON pt.`order_id` = ih.`order_id`
                                LEFT JOIN invoice AS i ON ih.`order_id` = i.`order_id`
                                WHERE i.`customer_id` ='.$user['id'].'
                                  AND used = 0
                                ORDER BY ih.create_date DESC');

                                    // where (pt.`transaction_status` = "capture" or pt.`transaction_status` = "settlement") and (pt.`fraud_status` = "accept" or pt.`fraud_status` = "") and i.`customer_id` ='.$user['id'].' and used = 0')

        foreach ($myVoucher as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
            
        }
        
        $res = array(
            'myVoucher'=>$myVoucher,
            'responeCode'=>0,
            'responeMessage'=>"Load Success",
            'status'=>"Success"
        );
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function detailMyVoucherActive(Request $request) 
    {
        $getReq = $request->only('id_history');
        $user = JWTAuth::parseToken()->authenticate();
        $myVoucher = DB::select('select v.*,ih.used,ih.qr_code from 
            invoice_history as ih 
            left join voucher as v 
                on ih.`voucher_id` = v.`voucher_id` 
            left join `payment_transaction` as pt 
                on pt.`order_id` = ih.`order_id`
            left join invoice as i 
                on ih.`order_id` = i.`order_id`
        where i.`customer_id` ='.$user['id'].' and used = 0 and ih.id = '.$getReq['id_history']);

        $code = 200;
        if (empty($myVoucher)) {
            $res = array("detailVoucher"=>$myVoucher,
                         'responeCode'=>1,
                         'responeMessage'=>"Data not Found",
                         'status'=>"Failed");
            $code = 404;
        }else{
            $link = "http://188.166.190.210/api_osai/public/qr/".$myVoucher[0]->qr_code.".png";
            $myVoucher[0]->image_qr = $link;
            $res = array("detailVoucher"=>$myVoucher,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");

            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$myVoucher[0]->company_id."/";
            $myVoucher[0]->image = $link.$myVoucher[0]->image;
            if (count($myVoucher)==0) {
                $res = array('responeCode'=>1,
                         'responeMessage'=>"Data not Found",
                         'status'=>"Failed");
                return response()->json($res, 404);
            }
            DB::table('voucher')
                ->where('id',$myVoucher[0]->voucher_id)
                ->increment('count_view');

            $res = array("detailVoucher"=>$myVoucher,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");    
        }
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function myVoucherHistory(Request $request) 
    {
        $user = JWTAuth::parseToken()->authenticate();
        $myVoucher = DB::select('select v.*,ih.used,ih.id as id_history from 
            invoice_history as ih 
            left join voucher as v 
                on ih.`voucher_id` = v.`voucher_id` 
            left join `payment_transaction` as pt 
                on pt.`order_id` = ih.`order_id`
            left join invoice as i 
                on ih.`order_id` = i.`order_id`
        where i.`customer_id` ='.$user['id'].' and used = 1');
        //(pt.`transaction_status` = "capture" or pt.`transaction_status` = "settlement") and (pt.`fraud_status` = "accept" or pt.`fraud_status` = "") and

        if(isset($myVoucher[0]->company_id)){
            foreach ($myVoucher as $key => $value) {
                if ($key=="image") {
                    $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
                    $value->image = $link.$value->image;
                }
            }
        }
        $code = 200;
        if (count($myVoucher)==0) {
            //return response()->json(['error' => 'error table'], 401);
            $res = array("myVoucher"=>$myVoucher,
                     'responeCode'=>1,
                     'responeMessage'=>"Not Found",
                     'status'=>"Failed");
            $code  = 404;
        }else{
            $res = array("myVoucher"=>$myVoucher,
                     'responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");
        }
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function voucherPoint(Request $request)
    {
        $getReq = $request->only('category_id','sort_by','sort_def','filter_by','filter_val','filter_between','filter_between_val');

        $custom_where = " where voucher.voucher_category = 1 and expired_date > '".date('Y-m-d H:i:s')."'";
        
        if ($request->has('filter_by') && $request->has('filter_val')) {
            $custom_where .= " and voucher.".$getReq['filter_by']." like '%".$getReq['filter_val']."%'";
        }
        $custom_order = "";
        if($request->has('sort_by') && $request->has('sort_def')){
            $custom_order = " order by ".$getReq['sort_by']." ".$getReq['sort_def'];
        }

        if($request->has('filter_between') && $request->has('filter_between_val')){
            $expBetween = explode(",", $getReq['filter_between_val']);
            $custom_where .= " and ".$getReq['filter_between']." between ".$expBetween[0]." and ".$expBetween[1];
        }

        
        $getData = DB::select(DB::raw("select voucher.*,merchant.name as merchant_name,company.name as company_name,company.company_id 
                from voucher 
                    join merchant 
                        on merchant.id = voucher.merchant_id
                    join company 
                        on company.company_id = merchant.company_id
                    $custom_where $custom_order
                "));
        unset($getData[0]->merchant_id);
        foreach ($getData as $key => $value) {
            $link = "http://188.166.190.210/osai/data_uploads/voucher/".$value->company_id."/";
            $value->image = $link.$value->image;
        }
        $code = 200;
        if (count($getData)==0) {
            $res = array("voucher"=>$getData,
                         'responeCode'=>1,
                         'responeMessage'=>"Data Not Found",
                         'status'=>"Error");
            $code = 404;
        }else{
            $res = array("voucher"=>$getData,
                         'responeCode'=>0,
                         'responeMessage'=>"Load Success",
                         'status'=>"Success");          
        }
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function sendPoint(Request $request) 
    {

        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('id_user_destination','total_point');

        $getPointSource = DB::table('users')
                            ->where('id',$user['id'])
                            ->first();

        $getPointDestination  = DB::table('users')
                                    ->where('id',$getReq['id_user_destination'])
                                    ->first();
        $code  = 200;
        if ($getPointSource->point < $getReq['total_point']) {
            $res = array('responeCode'=>0,
                         'responeMessage'=>"Your Point is not Enough",
                         'status'=>"Failed");
            $code = 404;
        }else{
            DB::table('users')
            ->where('id',$getReq['id_user_destination'])
            ->increment('point',$getReq['total_point']);

            DB::table('users')
                ->where('id',$user['id'])
                ->decrement('point',$getReq['total_point']);

            $getDataReceive = DB::table('users')
                                    ->where('id',$getReq['id_user_destination'])
                                    ->first();

            HlmHelper::sendNotif($getDataReceive->fcm_token,"Received ".$getReq['total_point']." Point from ".$user['name'],"Point Transfer");

            $point_history_in = array( 'type' =>5,
                                       'point'=>$getReq['total_point'],
                                       'customer_id'=>$getReq['id_user_destination'],
                                       'mutation'=>1,
                                       'sent_by'=>$user['id'],
                                       'create_date'=>date('Y-m-d H:i:s'),
                                       'change_date'=>date('Y-m-d H:i:s'));

            $point_history_out = array('type' =>6,
                                       'point'=>$getReq['total_point'],
                                       'customer_id'=>$user['id'],
                                       'mutation'=>2,
                                       'sent_by'=>$getReq['id_user_destination'],
                                       'create_date'=>date('Y-m-d H:i:s'),
                                       'change_date'=>date('Y-m-d H:i:s'));
            
            DB::table('point_history')->insert($point_history_in);   
            DB::table('point_history')->insert($point_history_out);    
            
            $res = array('responeCode'=>0,
                         'responeMessage'=>
                         "Transfer Success",
                         'status'=>"Success");    
        }
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function logoutUser($value='')
    {
        $user = JWTAuth::parseToken()->authenticate();
        DB::table('users')
            ->where('id',$user['id'])
            ->update(array('fcm_token'=>''));

        $res = array('responeCode'=>0,
                     'responeMessage'=>"Logout Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function listBankAccount($value='')
    {
        $user = JWTAuth::parseToken()->authenticate();
        $bankAccounts =  DB::table('users')
            ->join('bank_accounts','users.merchant_id','=','bank_accounts.merchant_id')
            ->where('users.id',$user['id'])
            ->select('bank_accounts.*')
            ->get();

        $res = array('bank_accounts'=>$bankAccounts,
                     'responeCode'=>0,
                     'responeMessage'=>"Logout Success",
                     'status'=>"Success");
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

	//end OSAI API USER

}
