<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Http\Controllers\Controller;
use DB;
use JWTAuth;
use App\Veritrans\Midtrans;
use App\Veritrans\Veritrans;
use App\Helper\HlmHelper;
use QRcode;

class SnapController extends Controller
{
    public function __construct()
    {   
        Midtrans::$serverKey = 'VT-server-PlH8wf3EGayGQWvNLixchX_e';
        //set is production to true for production mode
        Midtrans::$isProduction = false;
    }

    public function snap()
    {
        return view('snap_checkout');
    }

    public function genQr(){

        $PNG_TEMP_DIR = $_SERVER['DOCUMENT_ROOT']."/api_osai/public/qr/";
        $res = $this->genCode();
        $filename = $PNG_TEMP_DIR.$res.'.png';
        QRcode::png($res, $filename, "M", "20", 2);    

        return $res;
    }

    public function genCode()
    {
        $chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 10; $i++) {
         $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        return $res;
    }

    public function saveTransaction(Request $request) 
    {
        $midtrans = new Midtrans;
        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('voucher_id','total_order');
        if (!isset($_GET['voucher_id']) && !isset($_GET['total_order'])) {
            echo "set voucher_id sama total_order cuk";
            die();
        }
        $getReq['voucher_id'] = $_GET['voucher_id'];
        $getReq['total_order'] = $_GET['total_order'];


        // if (empty($getReq)) {
        //     $getReq['voucher_id']  = 'VCR-0000006';
        //     $getReq['total_order'] = 3;
        // }
        
        $getVoucher = DB::table('voucher')->where('voucher_id',$getReq['voucher_id'])->first();
        $order_id = HlmHelper::genOrderId($user['id']);
        if (count($getVoucher)==0) {
            $res = array(
            	'responeCode'=>1,
            	'responeMessage'=>"Voucher Item not found",
                'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        if ($getVoucher->stock==0 || $getVoucher->stock<$getReq['total_order']) {
            $res = array(
            	'responeCode'=>1,
                'responeMessage'=>"Stock not enough",
                'status'=>"Failed"
            );
            return response()->json($res,404);
        }

        $saveToOrder = array(
        	'voucher_id'=> $getReq['voucher_id'],
			'company_id' =>$getVoucher->company_id,
			'merchant_id'=>$getVoucher->merchant_id,
			'customer_id'=>$user['id'],
			'order_id'=>$order_id,
			'order_date'=>date('Y-m-d H:i:s'),
			'total'=>$getReq['total_order'],
			'total_price'=>$getReq['total_order']*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
			'create_date'=>date("Y-m-d H:i:s"),
			'change_date'=>date("Y-m-d H:i:s"),
			'change_by'=>$user['id']
        );
        if ($getVoucher->payment_type==4) {
            $saveToOrder['point'] = $getVoucher->point;
        }
        DB::table('tbl_order')->insert($saveToOrder);


        //$saveToPaymentTransaction = array('order_id'=>$order_id);
        //DB::table('payment_transaction')->insert($saveToPaymentTransaction);

        $transaction_details = array(
			'order_id'      => $order_id,
			'gross_amount'=>$getReq['total_order']*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100)
		);

        // Populate items
        $items = [
            array(
                'id'        => $getVoucher->voucher_id,
                'price'     => $getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
                'quantity'  => $getReq['total_order'],
                'name'      => $getVoucher->name
            )
        ];

        // Populate customer's billing address
        $billing_address = array(
            'first_name'    => $user['name'],
            'last_name'     => $user['last_name'],
            'address'       => $user['address'],
            'city'          => $user['city'],
            'postal_code'   => $user['postal_code'],
            'phone'         => $user['phone_number'],
            'country_code'  => 'IDN'
        );

        // Populate customer's Info
        $customer_details = array(
            'first_name'      => $user['name'],
            'last_name'       => $user['last_name'],
            'email'           => $user['email'],
            'phone'           => $user['phone_number']
        );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            echo '<html>
                      <head>
                      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
                      <script>
                        $( document ).ready(function() {
                            $("#pay-button").hide();
                            $("#pay-button").click();
                        });
                      </script>
                    <script type="text/javascript"
                                src="https://app.sandbox.midtrans.com/snap/snap.js"
                                data-client-key="VT-client-q3ohGoloqFoviGMW"></script> 
                    <style>
                    .app-container{
                        width : 100% !important;
                    }
                    .button-main{
                        width : auto !important;
                    }
                    </style>
                      </head>
                      <body>
                        <button id="pay-button">Pay!</button>
                        <script type="text/javascript">
                          var payButton = document.getElementById(\'pay-button\');
                          payButton.addEventListener(\'click\', function () {
                            snap.pay(\''.$snap_token.'\'); // store your snap token here
                          });
                        </script>
                      </body>
                    </html>';
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function token() 
    {
        error_log('masuk ke snap token dri ajax');
        $midtrans = new Midtrans;

        $transaction_details = array(
            'order_id'      => uniqid(),
            'gross_amount'  => 200000
        );

        // Populate items
        $items = [
            array(
                'id'        => 'item3',
                'price'     => 100000,
                'quantity'  => 1,
                'name'      => 'Adidas f50'
            ),
            array(
                'id'        => 'item3',
                'price'     => 50000,
                'quantity'  => 2,
                'name'      => 'Nike N90'
            )
        ];

        // Populate customer's billing address
        $billing_address = array(
            'first_name'    => "Andri",
            'last_name'     => "Setiawan",
            'address'       => "Karet Belakang 15A, Setiabudi.",
            'city'          => "Jakarta",
            'postal_code'   => "51161",
            'phone'         => "081322311801",
            'country_code'  => 'IDN'
        );

        // // Populate customer's shipping address
        // $shipping_address = array(
        //     'first_name'    => "John",
        //     'last_name'     => "Watson",
        //     'address'       => "Bakerstreet 221B.",
        //     'city'          => "Jakarta",
        //     'postal_code'   => "51162",
        //     'phone'         => "081322311801",
        //     'country_code'  => 'IDN'
        //     );

        // Populate customer's Info
        $customer_details = array(
            'first_name'      => "Hilman",
            'last_name'       => "Syafei",
            'email'           => "hilman@asdasd.com",
            'phone'           => "081322311801"
            //'billing_address' => $billing_address
            //'shipping_address'=> $shipping_address
        );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
            //echo $snap_token;
            echo '<html>
                      <head>
                      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
                      <script>
                        $( document ).ready(function() {
                            $("#pay-button").hide();
                            $("#pay-button").click();
                        });
                      </script>
                    <script type="text/javascript"
                                src="https://app.sandbox.midtrans.com/snap/snap.js"
                                data-client-key="VT-client-q3ohGoloqFoviGMW"></script> 
                    <style>
                    .app-container{
                        width : 100% !important;
                    }
                    .button-main{
                        width : auto !important;
                    }
                    </style>
                      </head>
                      <body>
                        <button id="pay-button">Pay!</button>
                        <script type="text/javascript">
                          var payButton = document.getElementById(\'pay-button\');
                          payButton.addEventListener(\'click\', function () {
                            snap.pay(\''.$snap_token.'\'); // store your snap token here
                          });
                        </script>
                      </body>
                    </html>';
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function finish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        echo $result->status_message . '<br>';
        echo 'RESULT <br><pre>';
        var_dump($result);
        echo '</pre>' ;
    }

    public function unfinish(Request $request)
    {
        echo "Mohon Maaf Transaksi Gagal Dilakukan";
    }

    public function notification()
    {
        $midtrans = new Midtrans;
        //echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $insertLogNotif = array('notif_data' =>$json_result);
        HlmHelper::insertData('log_notif_payment',$insertLogNotif);
        $result = json_decode($json_result,true);
        //echo "<pre>";print_r($result);die();
        //if($result){
        //    $notif = $midtrans->status($result->order_id);
        //}
        //print_r($notif);die();
        //error_log(print_r($result,TRUE));
        if (isset($result['permata_va_number'])) {
            $result['va_number'] = $result['permata_va_number'];
            $result['bank'] = 'permata';
            unset($result['permata_va_number']);
        }
        if (isset($result['va_numbers'])) {
            $result['bank'] = $result['va_numbers'][0]['bank'];
            $result['va_number'] = $result['va_numbers'][0]['va_number'];
            unset($result['va_numbers']);
            unset($result['payment_amounts']);
        }
        $first_notif = false;
        $checkdata = DB::table('payment_transaction')
        				->where('order_id',$result['order_id'])
        				->first();
        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($result);
            $first_notif = true;
            $checkdata = DB::table('payment_transaction')
            				->where('order_id',$result['order_id'])
            				->first();
        }else{
            DB::table('payment_transaction')
            	->where('order_id',$result['order_id'])
            	->update($result);    
        }
        
        if (($checkdata->fraud_status!=$result['fraud_status'] && $checkdata->transaction_status!=$result['transaction_status']) || $first_notif ) {

            $getOrder = DB::table('tbl_order')->where('order_id',$result['order_id'])->first();
            $getVoucher = DB::table('voucher')->where('voucher_id',$getOrder->voucher_id)->first();

            $checkInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();

            $total_order  = $getOrder->total;
            $voucher_add = 0;
            if ($getVoucher->count_buy!=0) {
                $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
                $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
            }
            if (count($checkInvoice)==0) {
                $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
                $saveToInvoice = array(
                	'voucher_id'=>$getVoucher->voucher_id,
					'invoice_number'=>$invoice_number,
					'company_id' =>$getVoucher->company_id,
					'merchant_id'=>$getVoucher->merchant_id,
					'customer_id'=>$getOrder->customer_id,
					'order_id'=>$getOrder->order_id,
					'order_date'=>date('Y-m-d H:i:s'),
					'total'=>$total_order,
					'bonus_voucher'=>$getVoucher->get_buy,
					'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
					'payment_type'=>$getVoucher->payment_type,
					'create_date'=>date("Y-m-d H:i:s"),
					'change_date'=>date("Y-m-d H:i:s"),
					'change_by'=>$getOrder->change_by
				);
                DB::table('invoice')->insert($saveToInvoice);

                for ($i=0; $i <$getOrder->total; $i++) { 
                    $saveToInvoiceHistory = array(
                    	'order_id' =>$getOrder->order_id,
						'name'=>$getVoucher->name,
						'invoice_number'=>$invoice_number,
						'voucher_id'=>$getVoucher->voucher_id,
						'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
						'bonus_point'=>$getVoucher->bonus_point,
						'payment_type'=>$getVoucher->payment_type,
						'voucher_category'=>$getVoucher->voucher_category,
						'expired_date'=>$getVoucher->expired_date,
						'create_date'=>date('Y-m-d H:i:s'),
						'change_date'=>date('Y-m-d H:i:s'),
						'change_by'=>$getOrder->change_by
					);
                        if ($getVoucher->payment_type==4) {
                            $saveToInvoiceHistory['point'] = $getVoucher->point;
                        }
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }

                //if ($getOrder->total_order>=$getVoucher->count_buy && $getVoucher->count_buy!=0 && $getVoucher->get_buy!=0) {
                for ($i=0; $i < $voucher_add; $i++) { 
                    $saveToInvoiceHistory = array(
                    	'order_id' =>$getOrder->order_id,
						'name'=>$getVoucher->name,
						'invoice_number'=>$invoice_number,
						'voucher_id'=>$getVoucher->voucher_id,
						'price'=>0,
						'point'=>0,
						'bonus_point'=>$getVoucher->bonus_point,
						'payment_type'=>$getVoucher->payment_type,
						'voucher_category'=>$getVoucher->voucher_category,
						'expired_date'=>$getVoucher->expired_date,
						'voucher_type'=>1,
						'create_date'=>date('Y-m-d H:i:s'),
						'change_date'=>date('Y-m-d H:i:s'),
						'change_by'=>$getOrder->change_by
					);
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }
            //}
            }

            $getInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();
            if (isset($result['fraud_status']) && isset($result['transaction_status']) ) {
                if ($result['fraud_status']=="accept" && $result['transaction_status']=="capture" && $getInvoice->status_invoice !=3) {
                    $arrUpdate = array(
                    	'stock'=>($getVoucher->stock-$getInvoice->total),
                        'sold'=>($getVoucher->sold+$getInvoice->total)
                    );
                    DB::table('voucher')
                    	->where('voucher_id',$getInvoice->voucher_id)
                    	->update($arrUpdate);
                    
                    DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));

                    $dataInvoiceHistory = DB::table('invoice_history')
                    						->where('order_id',$getInvoice->order_id)
                    						->get();

                    if ($dataInvoiceHistory[0]->qr_code=="" && $getVoucher->voucher_category!=1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $qrCode = $this->genQr();
                            $updateQr = array('qr_code' =>$qrCode);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateQr);

                            $point_history = array(
                            	'type' =>1,
								'point'=>$value->bonus_point,
								'mutation'=>1,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);

                            if($value->bonus_point>0 && $value->voucher_type!=1){
                                DB::table('point_history')->insert($point_history);  
                                DB::table('users')
                                	->where('id',$getInvoice->customer_id)
                                	->increment('point',$value->bonus_point);  
                            }
                            
                            
                        }
                    }elseif ($getVoucher->voucher_category==1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $updateUsed = array('used' =>1);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateUsed);

                            $point_history = array(
                            	'type' =>1,
								'point'=>$getVoucher->point_get,
								'mutation'=>3,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);
                            
                            DB::table('point_history')->insert($point_history);    
                            
                            DB::table('users')
                            	->where('id',$getInvoice->customer_id)
                            	->increment('point',$getVoucher->point_get);
                        }
                    }
                }
            }elseif ($result['payment_type']!="credit_card") {
                if ($result['transaction_status']=="settlement" && $getInvoice->status_invoice !=3) {

                    DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));

                    $arrUpdate = array(
                    	'stock'=>($getVoucher->stock-$getInvoice->total),
                        'sold'=>($getVoucher->sold+$getInvoice->total)
                    );

                    DB::table('voucher')
                    	->where('voucher_id',$getInvoice->voucher_id)
                    	->update($arrUpdate);

                    $dataInvoiceHistory = DB::table('invoice_history')
                    						->where('order_id',$getInvoice->order_id)
                    						->get();

                    if ($dataInvoiceHistory[0]->qr_code == "" && $getVoucher->voucher_category != 1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $qrCode = $this->genQr();
                            $updateQr = array('qr_code' =>$qrCode);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateQr);

                            $point_history = array(
								'type' =>1,
								'point'=>$value->bonus_point,
								'mutation'=>1,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);

                            if($value->bonus_point>0 && $value->voucher_type!=1){
                                DB::table('point_history')
                                	->insert($point_history);  

                                DB::table('users')
                                	->where('id',$getInvoice->customer_id)
                                	->increment('point',$value->bonus_point);  
                            }
                            
                        }
                    }elseif ($getVoucher->voucher_category==1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $updateUsed = array('used' =>1);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateUsed);

                            $point_history = array(
                            	'type' =>1,
								'point'=>$value->point_get,
								'mutation'=>3,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);
                            
                            DB::table('point_history')->insert($point_history);    
                            
                            DB::table('users')
                            	->where('id',$getInvoice->customer_id)
                            	->increment('point',$value->point_get);
                        }
                    }
                }
            }
        }

        //DB::table('payment_transaction')->insert($result);
        
        $transaction = $result['transaction_status'];
        $type = $result['payment_type'];
        $order_id = $result['order_id'];
        $fraud = $result['fraud_status'];

        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          	if ($type == 'credit_card'){
	            if($fraud == 'challenge'){
	            	// TODO set payment status in merchant's database to 'Challenge by FDS'
	            	// TODO merchant should decide whether this transaction is authorized or not in MAP
	            	echo "Transaction order_id: " . $order_id ." is challenged by FDS";
	            }else {
	             	// TODO set payment status in merchant's database to 'Success'
	             	echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
	            }
        	}
        }else if ($transaction == 'settlement'){
        	// TODO set payment status in merchant's database to 'Settlement'
        	echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
        }else if($transaction == 'pending'){
          	// TODO set payment status in merchant's database to 'Pending'
          	echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
        }else if ($transaction == 'deny') {
          	// TODO set payment status in merchant's database to 'Denied'
          	echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        }else if ($transaction == 'cancel'){
        	//TODO cancel for permata VA
        }else if($transaction == 'expire' || $transaction == 'Expired'){
        	//TODO expire for permata VA
        }

        // if ($transaction == 'capture') {
        //   // For credit card transaction, we need to check whether transaction is challenge by FDS or not
        //   if ($type == 'credit_card'){
        //     if($fraud == 'challenge'){
        //       // TODO set payment status in merchant's database to 'Challenge by FDS'
        //       // TODO merchant should decide whether this transaction is authorized or not in MAP
        //       echo "Transaction order_id: " . $order_id ." is challenged by FDS";
        //     }else {
        //       // TODO set payment status in merchant's database to 'Success'
        //       echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
        //     }
        //    }
        // }else if ($transaction == 'settlement'){
        //  	// TODO set payment status in merchant's database to 'Settlement'
        //   	echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
        //  }else if($transaction == 'pending'){
        //   	// TODO set payment status in merchant's database to 'Pending'
        //   	echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
        //  }else if ($transaction == 'deny') {
        //   	// TODO set payment status in merchant's database to 'Denied'
        //   	echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        // 	}
    }

    public function notificationNew(){
    	$midtrans = new Midtrans;
        $json_result = file_get_contents('php://input');
        $insertLogNotif = array('notif_data' =>$json_result);
        HlmHelper::insertData('log_notif_payment',$insertLogNotif);
        $result = json_decode($json_result,true);

        if (isset($result['permata_va_number'])) {
            $result['va_number'] = $result['permata_va_number'];
            $result['bank'] = 'permata';
            unset($result['permata_va_number']);
        }
        if (isset($result['va_numbers'])) {
            $result['bank'] = $result['va_numbers'][0]['bank'];
            $result['va_number'] = $result['va_numbers'][0]['va_number'];
            unset($result['va_numbers']);
            unset($result['payment_amounts']);
        }
        
        $checkdata = DB::table('payment_transaction')
        				->where('order_id',$result['order_id'])
        				->first();
        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($result);
            $checkdata = DB::table('payment_transaction')
            				->where('order_id',$result['order_id'])
            				->first();
        }else{
            DB::table('payment_transaction')
            	->where('order_id',$result['order_id'])
            	->update($result);    
        }

        $getOrder = DB::table('tbl_order')->where('order_id',$result['order_id'])->first();
        $getVoucher = DB::table('voucher')->where('voucher_id',$getOrder->voucher_id)->first();
        $checkInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();

        $total_order  = $getOrder->total;
        $voucher_add = 0;
        if ($getVoucher->count_buy!=0) {
            $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
            $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
        }
        if (count($checkInvoice)==0) {
            $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
            $saveToInvoice = array(
            	'voucher_id'=>$getVoucher->voucher_id,
				'invoice_number'=>$invoice_number,
				'company_id' =>$getVoucher->company_id,
				'merchant_id'=>$getVoucher->merchant_id,
				'customer_id'=>$getOrder->customer_id,
				'order_id'=>$getOrder->order_id,
				'order_date'=>date('Y-m-d H:i:s'),
				'total'=>$total_order,
				'bonus_voucher'=>$getVoucher->get_buy,
				'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
				'payment_type'=>$getVoucher->payment_type,
				'create_date'=>date("Y-m-d H:i:s"),
				'change_date'=>date("Y-m-d H:i:s"),
				'change_by'=>$getOrder->change_by
			);
            DB::table('invoice')->insert($saveToInvoice);

            for ($i=0; $i <$getOrder->total; $i++) { 
                $saveToInvoiceHistory = array(
                	'order_id' =>$getOrder->order_id,
					'name'=>$getVoucher->name,
					'invoice_number'=>$invoice_number,
					'voucher_id'=>$getVoucher->voucher_id,
					'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
					'bonus_point'=>$getVoucher->bonus_point,
					'payment_type'=>$getVoucher->payment_type,
					'voucher_category'=>$getVoucher->voucher_category,
					'expired_date'=>$getVoucher->expired_date,
					'create_date'=>date('Y-m-d H:i:s'),
					'change_date'=>date('Y-m-d H:i:s'),
					'change_by'=>$getOrder->change_by
				);
                    if ($getVoucher->payment_type==4) {
                        $saveToInvoiceHistory['point'] = $getVoucher->point;
                    }
                DB::table('invoice_history')->insert($saveToInvoiceHistory);
            }

            for ($i=0; $i < $voucher_add; $i++) { 
                $saveToInvoiceHistory = array(
                	'order_id' =>$getOrder->order_id,
					'name'=>$getVoucher->name,
					'invoice_number'=>$invoice_number,
					'voucher_id'=>$getVoucher->voucher_id,
					'price'=>0,
					'point'=>0,
					'bonus_point'=>$getVoucher->bonus_point,
					'payment_type'=>$getVoucher->payment_type,
					'voucher_category'=>$getVoucher->voucher_category,
					'expired_date'=>$getVoucher->expired_date,
					'voucher_type'=>1,
					'create_date'=>date('Y-m-d H:i:s'),
					'change_date'=>date('Y-m-d H:i:s'),
					'change_by'=>$getOrder->change_by
				);
                DB::table('invoice_history')->insert($saveToInvoiceHistory);
            }
        }

        $getInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();

        $transaction = $result['transaction_status'];
        $type = $result['payment_type'];
        $order_id = $result['order_id'];
        $fraud = $result['fraud_status'];

        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          	if ($type == 'credit_card'){
	            if($fraud == 'challenge'){
	            	// TODO set payment status in merchant's database to 'Challenge by FDS'
	            	// TODO merchant should decide whether this transaction is authorized or not in MAP
	            	echo "Transaction order_id: " . $order_id ." is challenged by FDS";
	            }else {
	             	// TODO set payment status in merchant's database to 'Success'
                    DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));
	            }
        	}
        }else if ($transaction == 'settlement'){
        	// TODO set payment status in merchant's database to 'Settlement'
        	if ($type!="credit_card") {
        		DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));
        	}
        }else if($transaction == 'pending'){
          	// TODO set payment status in merchant's database to 'Pending'
        }else if ($transaction == 'deny') {
          	// TODO set payment status in merchant's database to 'Denied'
          	DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>2));
        }else if ($transaction == 'cancel'){
        	//TODO cancel for permata VA
        	DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>2));
        }else if($transaction == 'expire' || $transaction == 'Expired'){
        	//TODO expire for permata VA
        	DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>2));
        }

        $dataInvoiceHistory = DB::table('invoice_history')
                    			->where('order_id',$getInvoice->order_id)
                    			->get();
        if ($dataInvoiceHistory[0]->qr_code=="" && $getVoucher->voucher_category!=1) {
        	//pengurangan stock
        	$arrUpdate = array(
            	'stock'=>($getVoucher->stock-$getInvoice->total),
                'sold'=>($getVoucher->sold+$getInvoice->total)
            );
            DB::table('voucher')
            	->where('voucher_id',$getInvoice->voucher_id)
            	->update($arrUpdate);
        	//end pengurangan stock
            	
            foreach ($dataInvoiceHistory as $key => $value) {
                $qrCode = $this->genQr();
                $updateQr = array('qr_code' =>$qrCode);
                DB::table('invoice_history')
                	->where('id',$value->id)
                	->update($updateQr);

                $point_history = array(
                	'type' =>1,
					'point'=>$value->bonus_point,
					'mutation'=>1,
					'customer_id'=>$getInvoice->customer_id,
					'invoice_number'=>$getInvoice->invoice_number
				);

                if($value->bonus_point>0 && $value->voucher_type!=1){
                    DB::table('point_history')->insert($point_history);  
                    DB::table('users')
                    	->where('id',$getInvoice->customer_id)
                    	->increment('point',$value->bonus_point);  
                }
                
                
            }
        }elseif ($getVoucher->voucher_category==1) {
            foreach ($dataInvoiceHistory as $key => $value) {
                $updateUsed = array('used' =>1);
                DB::table('invoice_history')
                	->where('id',$value->id)
                	->update($updateUsed);

                $point_history = array(
                	'type' =>1,
					'point'=>$getVoucher->point_get,
					'mutation'=>3,
					'customer_id'=>$getInvoice->customer_id,
					'invoice_number'=>$getInvoice->invoice_number
				);
                
                DB::table('point_history')->insert($point_history);    
                
                DB::table('users')
                	->where('id',$getInvoice->customer_id)
                	->increment('point',$getVoucher->point_get);
            }
        }
    }

    public function notificationTest()
    {
        $midtrans = new Midtrans;
        //echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $json_result = '{
		  "transaction_time": "2017-11-12 02:54:52",
		  "transaction_status": "capture",
		  "transaction_id": "83c7be6e-4db4-4923-a2ef-76a9ca196d25",
		  "status_message": "Veritrans payment notification",
		  "status_code": "200",
		  "signature_key": "411eb2b3cddd08fbedf9e4d7a4998783b1dabe9d8a4270683910835fc4e2efe96ac176bd7e745a4fc2bddc34e839cf50fe4cf392dc5ba78fc71e5d6cb5856d28",
		  "payment_type": "credit_card",
		  "order_id": "ORD-0000389",
		  "masked_card": "481111-1114",
		  "gross_amount": "3150.00",
		  "fraud_status": "accept",
		  "eci": "05",
		  "channel_response_message": "Approved",
		  "channel_response_code": "00",
		  "bank": "mandiri",
		  "approval_code": "1510430096305"
		}';
		$insertLogNotif = array('notif_data' =>$json_result);
        $result = json_decode($json_result,true);
        HlmHelper::insertData('log_notif_payment',$insertLogNotif);
        //echo "<pre>";print_r($result);die();
        //if($result){
        //    $notif = $midtrans->status($result->order_id);
        //}
        //print_r($notif);die();
        //error_log(print_r($result,TRUE));
        if (isset($result['permata_va_number'])) {
            $result['va_number'] = $result['permata_va_number'];
            $result['bank'] = 'permata';
            unset($result['permata_va_number']);
        }
        if (isset($result['va_numbers'])) {
            $result['bank'] = $result['va_numbers'][0]['bank'];
            $result['va_number'] = $result['va_numbers'][0]['va_number'];
            unset($result['va_numbers']);
            unset($result['payment_amounts']);
        }
        $first_notif = false;
        $checkdata = DB::table('payment_transaction')
        				->where('order_id',$result['order_id'])
        				->first();
        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($result);
            $first_notif = true;
            $checkdata = DB::table('payment_transaction')
            				->where('order_id',$result['order_id'])
            				->first();
        }else{
            DB::table('payment_transaction')
            	->where('order_id',$result['order_id'])
            	->update($result);    
        }
        
        if (($checkdata->fraud_status!=$result['fraud_status'] && $checkdata->transaction_status!=$result['transaction_status']) || $first_notif ) {

            $getOrder = DB::table('tbl_order')
            				->where('order_id',$result['order_id'])
            				->first();

            $getVoucher = DB::table('voucher')
            				->where('voucher_id',$getOrder->voucher_id)
            				->first();

            $checkInvoice = DB::table('invoice')
            					->where('order_id',$result['order_id'])
            					->first();

            $total_order  = $getOrder->total;
            $voucher_add = 0;
            if ($getVoucher->count_buy!=0) {
                $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
                $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
            }
            if (count($checkInvoice)==0) {
                $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
                $saveToInvoice = array(
                	'voucher_id'=>$getVoucher->voucher_id,
					'invoice_number'=>$invoice_number,
					'company_id' =>$getVoucher->company_id,
					'merchant_id'=>$getVoucher->merchant_id,
					'customer_id'=>$getOrder->customer_id,
					'order_id'=>$getOrder->order_id,
					'order_date'=>date('Y-m-d H:i:s'),
					'total'=>$total_order,
					'bonus_voucher'=>$getVoucher->get_buy,
					'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
					'payment_type'=>$getVoucher->payment_type,
					'create_date'=>date("Y-m-d H:i:s"),
					'change_date'=>date("Y-m-d H:i:s"),
					'change_by'=>$getOrder->change_by
				);
                DB::table('invoice')->insert($saveToInvoice);

                for ($i=0; $i <$getOrder->total; $i++) { 
                    $saveToInvoiceHistory = array(
                    	'order_id' =>$getOrder->order_id,
						'name'=>$getVoucher->name,
						'invoice_number'=>$invoice_number,
						'voucher_id'=>$getVoucher->voucher_id,
						'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
						'bonus_point'=>$getVoucher->bonus_point,
						'payment_type'=>$getVoucher->payment_type,
						'voucher_category'=>$getVoucher->voucher_category,
						'expired_date'=>$getVoucher->expired_date,
						'create_date'=>date('Y-m-d H:i:s'),
						'change_date'=>date('Y-m-d H:i:s'),
						'change_by'=>$getOrder->change_by
					);
                        if ($getVoucher->payment_type==4) {
                            $saveToInvoiceHistory['point'] = $getVoucher->point;
                        }
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }

                //if ($getOrder->total_order>=$getVoucher->count_buy && $getVoucher->count_buy!=0 && $getVoucher->get_buy!=0) {
                for ($i=0; $i < $voucher_add; $i++) { 
                    $saveToInvoiceHistory = array(
                    	'order_id' =>$getOrder->order_id,
						'name'=>$getVoucher->name,
						'invoice_number'=>$invoice_number,
						'voucher_id'=>$getVoucher->voucher_id,
						'price'=>0,
						'point'=>0,
						'bonus_point'=>$getVoucher->bonus_point,
						'payment_type'=>$getVoucher->payment_type,
						'voucher_category'=>$getVoucher->voucher_category,
						'expired_date'=>$getVoucher->expired_date,
						'voucher_type'=>1,
						'create_date'=>date('Y-m-d H:i:s'),
						'change_date'=>date('Y-m-d H:i:s'),
						'change_by'=>$getOrder->change_by
					);
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }
            //}
            }

            $getInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();
            if (isset($result['fraud_status']) && isset($result['transaction_status']) ) {
                if ($result['fraud_status']=="accept" && $result['transaction_status']=="capture" && $getInvoice->status_invoice !=3) {
                    $arrUpdate = array(
                    	'stock'=>($getVoucher->stock-$getInvoice->total),
                        'sold'=>($getVoucher->sold+$getInvoice->total)
                    );
                    DB::table('voucher')
                    	->where('voucher_id',$getInvoice->voucher_id)
                    	->update($arrUpdate);
                    
                    DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));

                    $dataInvoiceHistory = DB::table('invoice_history')
                    						->where('order_id',$getInvoice->order_id)
                    						->get();

                    if ($dataInvoiceHistory[0]->qr_code=="" && $getVoucher->voucher_category!=1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $qrCode = $this->genQr();
                            $updateQr = array('qr_code' =>$qrCode);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateQr);

                            $point_history = array(
                            	'type' =>1,
								'point'=>$value->bonus_point,
								'mutation'=>1,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);

                            if($value->bonus_point>0 && $value->voucher_type!=1){
                                DB::table('point_history')->insert($point_history);  
                                DB::table('users')
                                	->where('id',$getInvoice->customer_id)
                                	->increment('point',$value->bonus_point);  
                            }
                            
                            
                        }
                    }elseif ($getVoucher->voucher_category==1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $updateUsed = array('used' =>1);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateUsed);

                            $point_history = array(
                            	'type' =>1,
								'point'=>$getVoucher->point_get,
								'mutation'=>3,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);
                            
                            DB::table('point_history')->insert($point_history);    
                            
                            DB::table('users')
                            	->where('id',$getInvoice->customer_id)
                            	->increment('point',$getVoucher->point_get);
                        }
                    }
                }
            }elseif ($result['payment_type']!="credit_card") {
                if ($result['transaction_status']=="settlement" && $getInvoice->status_invoice !=3) {

                    DB::table('invoice')
                    	->where('order_id',$getInvoice->order_id)
                    	->update(array('status_invoice'=>3));

                    $arrUpdate = array(
                    	'stock'=>($getVoucher->stock-$getInvoice->total),
                        'sold'=>($getVoucher->sold+$getInvoice->total)
                    );

                    DB::table('voucher')
                    	->where('voucher_id',$getInvoice->voucher_id)
                    	->update($arrUpdate);

                    $dataInvoiceHistory = DB::table('invoice_history')
                    						->where('order_id',$getInvoice->order_id)
                    						->get();

                    if ($dataInvoiceHistory[0]->qr_code == "" && $getVoucher->voucher_category != 1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $qrCode = $this->genQr();
                            $updateQr = array('qr_code' =>$qrCode);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateQr);

                            $point_history = array(
								'type' =>1,
								'point'=>$value->bonus_point,
								'mutation'=>1,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);

                            if($value->bonus_point>0 && $value->voucher_type!=1){
                                DB::table('point_history')
                                	->insert($point_history);  

                                DB::table('users')
                                	->where('id',$getInvoice->customer_id)
                                	->increment('point',$value->bonus_point);  
                            }
                            
                        }
                    }elseif ($getVoucher->voucher_category==1) {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $updateUsed = array('used' =>1);
                            DB::table('invoice_history')
                            	->where('id',$value->id)
                            	->update($updateUsed);

                            $point_history = array(
                            	'type' =>1,
								'point'=>$value->point_get,
								'mutation'=>3,
								'customer_id'=>$getInvoice->customer_id,
								'invoice_number'=>$getInvoice->invoice_number
							);
                            
                            DB::table('point_history')->insert($point_history);    
                            
                            DB::table('users')
                            	->where('id',$getInvoice->customer_id)
                            	->increment('point',$value->point_get);
                        }
                    }
                }
            }
        }
        //DB::table('payment_transaction')->insert($result);
        
        // $transaction = $result['transaction_status'];
        // $type = $result['payment_type'];
        // $order_id = $result['order_id'];
        // $fraud = $result['fraud_status'];

        // if ($transaction == 'capture') {
        //   // For credit card transaction, we need to check whether transaction is challenge by FDS or not
        //   if ($type == 'credit_card'){
        //     if($fraud == 'challenge'){
        //       // TODO set payment status in merchant's database to 'Challenge by FDS'
        //       // TODO merchant should decide whether this transaction is authorized or not in MAP
        //       echo "Transaction order_id: " . $order_id ." is challenged by FDS";
        //       } 
        //       else {
        //       // TODO set payment status in merchant's database to 'Success'
        //       echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
        //       }
        //     }
        //   }
        // else if ($transaction == 'settlement'){
        //   // TODO set payment status in merchant's database to 'Settlement'
        //   echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
        //   } 
        //   else if($transaction == 'pending'){
        //   // TODO set payment status in merchant's database to 'Pending'
        //   echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
        //   } 
        //   else if ($transaction == 'deny') {
        //   // TODO set payment status in merchant's database to 'Denied'
        //   echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        // }
    }

    // public function notificationTest()
    // {
    //     $midtrans = new Midtrans;
    //     //echo 'test notification handler';
    //     $json_result = file_get_contents('php://input');
    //     $json_result = '{
    //                         "transaction_time": "2017-11-06 12:24:44",
    //                         "transaction_status": "capture",
    //                         "transaction_id": "f7fb59d7-aaaa-4b54-875c-a5c398da93ad",
    //                         "status_message": "Veritrans payment notification",
    //                         "status_code": "200",
    //                         "signature_key": "3edff5a0b4b5f8d3ee065fc851dd255988f3c87360e2540463023c09e38cb03d08426c7b81e69b5c6e3bc54ec9a92bd824a860427499b90ac7156b6289fef7bc",
    //                         "payment_type": "credit_card",
    //                         "order_id": "ORD-0000349",
    //                         "masked_card": "481111-1114",
    //                         "gross_amount": "180000.00",
    //                         "fraud_status": "accept",
    //                         "eci": "05",
    //                         "bank": "mandiri",
    //                         "approval_code": "1509945884979"
    //                     }';
    //     $result = json_decode($json_result,true);
    //     //echo "<pre>";print_r($result);die();
    //     //if($result){
    //     //    $notif = $midtrans->status($result->order_id);
    //     //}
    //     //print_r($notif);die();
    //     //error_log(print_r($result,TRUE));
    //     if (isset($result['permata_va_number'])) {
    //         $result['va_number'] = $result['permata_va_number'];
    //         $result['bank'] = 'permata';
    //         unset($result['permata_va_number']);
    //     }
    //     if (isset($result['va_numbers'])) {
    //         $result['bank'] = $result['va_numbers'][0]['bank'];
    //         $result['va_number'] = $result['va_numbers'][0]['va_number'];
    //         unset($result['va_numbers']);
    //         unset($result['payment_amounts']);
    //     }
    //     $first_notif = false;
    //     $checkdata = DB::table('payment_transaction')->where('order_id',$result['order_id'])->first();
    //     if (count($checkdata)==0) {
    //         DB::table('payment_transaction')->insert($result);
    //         $first_notif = true;
    //         $checkdata = DB::table('payment_transaction')->where('order_id',$result['order_id'])->first();
    //     }else{
    //         DB::table('payment_transaction')->where('order_id',$result['order_id'])->update($result);    
    //     }
        
    //     if (($checkdata->fraud_status!=$result['fraud_status'] && $checkdata->transaction_status!=$result['transaction_status']) || $first_notif ) {

    //         $getOrder = DB::table('tbl_order')->where('order_id',$result['order_id'])->first();
    //         $getVoucher = DB::table('voucher')->where('voucher_id',$getOrder->voucher_id)->first();

    //         $checkInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();

    //         $total_order  = $getOrder->total;
    //         $voucher_add = 0;
    //         if ($getVoucher->count_buy!=0) {
    //             $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
    //             $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
    //         }
    //         if (count($checkInvoice)==0) {
    //             $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
    //             $saveToInvoice = array('voucher_id'=>$getVoucher->voucher_id,
    //                                    'invoice_number'=>$invoice_number,
    //                                    'company_id' =>$getVoucher->company_id,
    //                                    'merchant_id'=>$getVoucher->merchant_id,
    //                                    'customer_id'=>$getOrder->customer_id,
    //                                    'order_id'=>$getOrder->order_id,
    //                                    'order_date'=>date('Y-m-d H:i:s'),
    //                                    'total'=>$total_order,
    //                                    'bonus_voucher'=>$getVoucher->get_buy,
    //                                    'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
    //                                    'payment_type'=>$getVoucher->payment_type,
    //                                    'create_date'=>date("Y-m-d H:i:s"),
    //                                    'change_date'=>date("Y-m-d H:i:s"),
    //                                    'change_by'=>$getOrder->change_by);
    //             DB::table('invoice')->insert($saveToInvoice);

    //             for ($i=0; $i <$getOrder->total; $i++) { 
    //                 $saveToInvoiceHistory = array('order_id' =>$getOrder->order_id,
    //                                               'name'=>$getVoucher->name,
    //                                               'invoice_number'=>$invoice_number,
    //                                               'voucher_id'=>$getVoucher->voucher_id,
    //                                               'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
    //                                               'point'=>$getVoucher->point,
    //                                               'bonus_point'=>$getVoucher->bonus_point,
    //                                               'payment_type'=>$getVoucher->payment_type,
    //                                               'voucher_category'=>$getVoucher->voucher_category,
    //                                               'expired_date'=>$getVoucher->expired_date,
    //                                               'create_date'=>date('Y-m-d H:i:s'),
    //                                               'change_date'=>date('Y-m-d H:i:s'),
    //                                               'change_by'=>$getOrder->change_by);
    //                 DB::table('invoice_history')->insert($saveToInvoiceHistory);
    //             }

    //             //if ($getOrder->total_order>=$getVoucher->count_buy && $getVoucher->count_buy!=0 && $getVoucher->get_buy!=0) {
    //             for ($i=0; $i < $voucher_add; $i++) { 
    //                 $saveToInvoiceHistory = array('order_id' =>$getOrder->order_id,
    //                                               'name'=>$getVoucher->name,
    //                                               'invoice_number'=>$invoice_number,
    //                                               'voucher_id'=>$getVoucher->voucher_id,
    //                                               'price'=>0,
    //                                               'point'=>$getVoucher->point,
    //                                               'bonus_point'=>$getVoucher->bonus_point,
    //                                               'payment_type'=>$getVoucher->payment_type,
    //                                               'voucher_category'=>$getVoucher->voucher_category,
    //                                               'expired_date'=>$getVoucher->expired_date,
    //                                               'voucher_type'=>1,
    //                                               'create_date'=>date('Y-m-d H:i:s'),
    //                                               'change_date'=>date('Y-m-d H:i:s'),
    //                                               'change_by'=>$getOrder->change_by);
    //                 DB::table('invoice_history')->insert($saveToInvoiceHistory);
    //             }
    //         //}
    //         }

    //         $getInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();
    //         if (isset($result['fraud_status']) && isset($result['transaction_status']) ) {
    //             if ($result['fraud_status']=="accept" && $result['transaction_status']=="capture" && $getInvoice->status_invoice !=3) {
    //                 $arrUpdate = array('stock'=>($getVoucher->stock-$getInvoice->total),
    //                                    'sold'=>($getVoucher->sold+$getInvoice->total));
    //                 DB::table('voucher')->where('voucher_id',$getInvoice->voucher_id)->update($arrUpdate);
                    
    //                 DB::table('invoice')->where('order_id',$getInvoice->order_id)->update(array('status_invoice'=>3));

    //                 $dataInvoiceHistory = DB::table('invoice_history')->where('order_id',$getInvoice->order_id)->get();
    //                 if ($dataInvoiceHistory[0]->qr_code=="" && $getVoucher->voucher_category!=1) {
    //                     foreach ($dataInvoiceHistory as $key => $value) {
    //                         $qrCode = $this->genQr();
    //                         $updateQr = array('qr_code' =>$qrCode);
    //                         DB::table('invoice_history')->where('id',$value->id)->update($updateQr);

    //                         $point_history = array('type' =>1,
    //                                                'point'=>$value->bonus_point,
    //                                                'mutation'=>1,
    //                                                'customer_id'=>$getInvoice->customer_id,
    //                                                'invoice_number'=>$getInvoice->invoice_number);
    //                         if($value->bonus_point>0){
    //                             DB::table('point_history')->insert($point_history);    
    //                         }
                            
    //                         DB::table('users')->where('id',$getInvoice->customer_id)->increment('point',$value->bonus_point);
    //                     }
    //                 }elseif ($getVoucher->voucher_category==1) {
    //                     foreach ($dataInvoiceHistory as $key => $value) {
    //                         $updateUsed = array('used' =>1);
    //                         DB::table('invoice_history')->where('id',$value->id)->update($updateUsed);

    //                         $point_history = array('type' =>1,
    //                                                'point'=>$getVoucher->point_get,
    //                                                'mutation'=>3,
    //                                                'customer_id'=>$getInvoice->customer_id,
    //                                                'invoice_number'=>$getInvoice->invoice_number);
                            
    //                         DB::table('point_history')->insert($point_history);    
                            
    //                         DB::table('users')->where('id',$getInvoice->customer_id)->increment('point',$getVoucher->point_get);
    //                     }
    //                 }
    //             }
    //         }elseif ($result['payment_type']!="credit_card") {
    //             if ($result['transaction_status']=="settlement" && $getInvoice->status_invoice !=3) {

    //                 DB::table('invoice')->where('order_id',$getInvoice->order_id)->update(array('status_invoice'=>3));

    //                 $arrUpdate = array('stock'=>($getVoucher->stock-$getInvoice->total),
    //                                    'sold'=>($getVoucher->sold+$getInvoice->total));
    //                 DB::table('voucher')->where('voucher_id',$getInvoice->voucher_id)->update($arrUpdate);
    //                 $dataInvoiceHistory = DB::table('invoice_history')->where('order_id',$getInvoice->order_id)->get();
    //                 if ($dataInvoiceHistory[0]->qr_code=="" && $getVoucher->voucher_category!=1) {
    //                     foreach ($dataInvoiceHistory as $key => $value) {
    //                         $qrCode = $this->genQr();
    //                         $updateQr = array('qr_code' =>$qrCode);
    //                         DB::table('invoice_history')->where('id',$value->id)->update($updateQr);

    //                         $point_history = array('type' =>1,
    //                                                'point'=>$value->bonus_point,
    //                                                'mutation'=>1,
    //                                                'customer_id'=>$getInvoice->customer_id,
    //                                                'invoice_number'=>$getInvoice->invoice_number);
    //                         if($value->bonus_point>0){
    //                             DB::table('point_history')->insert($point_history);    
    //                         }
    //                         DB::table('users')->where('id',$getInvoice->customer_id)->increment('point',$value->bonus_point);
    //                     }
    //                 }elseif ($getVoucher->voucher_category==1) {
    //                     foreach ($dataInvoiceHistory as $key => $value) {
    //                         $updateUsed = array('used' =>1);
    //                         DB::table('invoice_history')->where('id',$value->id)->update($updateUsed);

    //                         $point_history = array('type' =>1,
    //                                                'point'=>$value->point_get,
    //                                                'mutation'=>3,
    //                                                'customer_id'=>$getInvoice->customer_id,
    //                                                'invoice_number'=>$getInvoice->invoice_number);
                            
    //                         DB::table('point_history')->insert($point_history);    
                            
    //                         DB::table('users')->where('id',$getInvoice->customer_id)->increment('point',$value->point_get);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     //DB::table('payment_transaction')->insert($result);
        
    //     // $transaction = $result['transaction_status'];
    //     // $type = $result['payment_type'];
    //     // $order_id = $result['order_id'];
    //     // $fraud = $result['fraud_status'];

    //     // if ($transaction == 'capture') {
    //     //   // For credit card transaction, we need to check whether transaction is challenge by FDS or not
    //     //   if ($type == 'credit_card'){
    //     //     if($fraud == 'challenge'){
    //     //       // TODO set payment status in merchant's database to 'Challenge by FDS'
    //     //       // TODO merchant should decide whether this transaction is authorized or not in MAP
    //     //       echo "Transaction order_id: " . $order_id ." is challenged by FDS";
    //     //       } 
    //     //       else {
    //     //       // TODO set payment status in merchant's database to 'Success'
    //     //       echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
    //     //       }
    //     //     }
    //     //   }
    //     // else if ($transaction == 'settlement'){
    //     //   // TODO set payment status in merchant's database to 'Settlement'
    //     //   echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
    //     //   } 
    //     //   else if($transaction == 'pending'){
    //     //   // TODO set payment status in merchant's database to 'Pending'
    //     //   echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
    //     //   } 
    //     //   else if ($transaction == 'deny') {
    //     //   // TODO set payment status in merchant's database to 'Denied'
    //     //   echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
    //     // }
    // }

    public function notificationTestOld()
    {
        $midtrans = new Midtrans;
        //echo 'test notification handler';
        $json_result = '{
              "transaction_time": "2017-10-30 15:11:10",
              "transaction_status": "settlement",
              "transaction_id": "370d1d29-63a0-4aef-bae8-64f54cc2cafe",
              "status_message": "Veritrans payment notification",
              "status_code": "200",
              "signature_key": "288ae0e2eb63007981577413fdd4f43e3dc01f48aab5dc51b09233d94cc0d9472166bd104424898879b0e04eb8c4714ca708c36835ead1804b14500444f3b85f",
              "payment_type": "credit_card",
              "order_id": "VC-0000164",
              "masked_card": "481111-1114",
              "gross_amount": "360000.00",
              "fraud_status": "accept",
              "eci": "05",
              "bank": "mandiri",
              "approval_code": "1509351070432"
            }';
        $result = json_decode($json_result,true);
        //echo "<pre>";print_r($result);die();
        //if($result){
        //    $notif = $midtrans->status($result->order_id);
        //}
        //print_r($notif);die();
        //error_log(print_r($result,TRUE));
        if (isset($result['permata_va_number'])) {
            $result['va_number'] = $result['permata_va_number'];
            $result['bank'] = 'permata';
            unset($result['permata_va_number']);
        }
        if (isset($result['va_numbers'])) {
            $result['bank'] = $result['va_numbers'][0]['bank'];
            $result['va_number'] = $result['va_numbers'][0]['va_number'];
            unset($result['va_numbers']);
            unset($result['payment_amounts']);
        }

        $checkdata = DB::table('payment_transaction')->where('order_id',$result['order_id'])->first();
        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($result);
        }else{
            DB::table('payment_transaction')->where('order_id',$result['order_id'])->update($result);    
        }
        if ($checkdata->fraud_status!=$result['fraud_status'] && $checkdata->transaction_status!=$result['transaction_status']) {
            $getOrder = DB::table('tbl_order')->where('order_id',$result['order_id'])->first();
            $getVoucher = DB::table('voucher')->where('voucher_id',$getOrder->voucher_id)->first();

            $checkInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();
            if (count($checkInvoice)==0) {
                $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
                $saveToInvoice = array('voucher_id'=>$getVoucher->voucher_id,
                                       'invoice_number'=>$invoice_number,
                                       'company_id' =>$getVoucher->company_id,
                                       'merchant_id'=>$getVoucher->merchant_id,
                                       'customer_id'=>$getOrder->customer_id,
                                       'order_id'=>$getOrder->order_id,
                                       'order_date'=>date('Y-m-d H:i:s'),
                                       'total'=>$getOrder->total,
                                       'bonus_voucher'=>$getVoucher->get_buy,
                                       'total_price'=>$getOrder->total*$getVoucher->price-($getVoucher->discount/100*$getOrder->total*$getVoucher->price),
                                       'payment_type'=>$getVoucher->payment_type,
                                       'create_date'=>date("Y-m-d H:i:s"),
                                       'change_date'=>date("Y-m-d H:i:s"),
                                       'change_by'=>$getOrder->change_by);
                DB::table('invoice')->insert($saveToInvoice);

                for ($i=0; $i <$getOrder->total; $i++) { 
                    $saveToInvoiceHistory = array('order_id' =>$getOrder->order_id,
                                                  'name'=>$getVoucher->name,
                                                  'invoice_number'=>$invoice_number,
                                                  'voucher_id'=>$getVoucher->voucher_id,
                                                  'price'=>$getVoucher->price-($getVoucher->discount/100*$getVoucher->price),
                                                  'point'=>$getVoucher->point,
                                                  'bonus_point'=>$getVoucher->bonus_point,
                                                  'payment_type'=>$getVoucher->payment_type,
                                                  'voucher_category'=>$getVoucher->voucher_category,
                                                  'expired_date'=>$getVoucher->expired_date,
                                                  'create_date'=>date('Y-m-d H:i:s'),
                                                  'change_date'=>date('Y-m-d H:i:s'),
                                                  'change_by'=>$getOrder->change_by);
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }

                if ($getOrder->total_order>=$getVoucher->count_buy && $getVoucher->count_buy!=0 && $getVoucher->get_buy!=0) {
                for ($i=0; $i < floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy; $i++) { 
                    $saveToInvoiceHistory = array('order_id' =>$getOrder->order_id,
                                                  'name'=>$getVoucher->name,
                                                  'invoice_number'=>$invoice_number,
                                                  'voucher_id'=>$getVoucher->voucher_id,
                                                  'price'=>$getVoucher->price,
                                                  'point'=>$getVoucher->point,
                                                  'bonus_point'=>$getVoucher->bonus_point,
                                                  'payment_type'=>$getVoucher->payment_type,
                                                  'voucher_category'=>$getVoucher->voucher_category,
                                                  'expired_date'=>$getVoucher->expired_date,
                                                  'voucher_type'=>1,
                                                  'create_date'=>date('Y-m-d H:i:s'),
                                                  'change_date'=>date('Y-m-d H:i:s'),
                                                  'change_by'=>$getOrder->change_by);
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }
            }
            }

            $getInvoice = DB::table('invoice')->where('order_id',$result['order_id'])->first();
            if (isset($result['fraud_status']) && isset($result['transaction_status']) ) {
                if ($result['fraud_status']=="accept" && $result['transaction_status']=="capture") {
                    
                    $arrUpdate = array('stock'=>($getVoucher->stock-$getInvoice->total),
                                       'sold'=>($getVoucher->sold+$getInvoice->total));
                    DB::table('voucher')->where('voucher_id',$getInvoice->voucher_id)->update($arrUpdate);
                    
                    $dataInvoiceHistory = DB::table('invoice_history')->where('order_id',$getInvoice->order_id)->get();
                    if ($dataInvoiceHistory[0]->qr_code=="") {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $qrCode = $this->genQr();
                            $updateQr = array('qr_code' =>$qrCode);
                            DB::table('invoice_history')->where('id',$value->id)->update($updateQr);

                            $point_history = array('type' =>1,
                                                   'point'=>$value->bonus_point,
                                                   'customer_id'=>$getInvoice->customer_id,
                                                   'invoice_number'=>$getInvoice->invoice_number);
                            
                            if($value->bonus_point>0){
                                DB::table('point_history')->insert($point_history);    
                            }
                            
                            DB::table('users')->where('id',$getInvoice->customer_id)->increment('point',$value->bonus_point);
                        }
                    }
                }
            }elseif ($result['payment_type']!="credit_card") {
                if ($result['transaction_status']=="settlement") {
                    
                    $arrUpdate = array('stock'=>($getVoucher->stock-$getInvoice->total),
                                       'sold'=>($getVoucher->sold+$getInvoice->total));
                    DB::table('voucher')->where('voucher_id',$getInvoice->voucher_id)->update($arrUpdate);
                    $dataInvoiceHistory = DB::table('invoice_history')->where('order_id',$getInvoice->order_id)->get();
                    if ($dataInvoiceHistory[0]->qr_code=="") {
                        foreach ($dataInvoiceHistory as $key => $value) {
                            $qrCode = $this->genQr();
                            $updateQr = array('qr_code' =>$qrCode);
                            DB::table('invoice_history')->where('id',$value->id)->update($updateQr);

                            $point_history = array('type' =>1,
                                                   'point'=>$value->bonus_point,
                                                   'customer_id'=>$getInvoice->customer_id,
                                                   'invoice_number'=>$getInvoice->invoice_number);
                            if($value->bonus_point>0){
                                DB::table('point_history')->insert($point_history);    
                            }
                            DB::table('users')->where('id',$getInvoice->customer_id)->increment('point',$value->bonus_point);
                        }
                    }
                }
            }
        }
        //DB::table('payment_transaction')->insert($result);
        
        // $transaction = $result['transaction_status'];
        // $type = $result['payment_type'];
        // $order_id = $result['order_id'];
        // $fraud = $result['fraud_status'];

        // if ($transaction == 'capture') {
        //   // For credit card transaction, we need to check whether transaction is challenge by FDS or not
        //   if ($type == 'credit_card'){
        //     if($fraud == 'challenge'){
        //       // TODO set payment status in merchant's database to 'Challenge by FDS'
        //       // TODO merchant should decide whether this transaction is authorized or not in MAP
        //       echo "Transaction order_id: " . $order_id ." is challenged by FDS";
        //       } 
        //       else {
        //       // TODO set payment status in merchant's database to 'Success'
        //       echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
        //       }
        //     }
        //   }
        // else if ($transaction == 'settlement'){
        //   // TODO set payment status in merchant's database to 'Settlement'
        //   echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
        //   } 
        //   else if($transaction == 'pending'){
        //   // TODO set payment status in merchant's database to 'Pending'
        //   echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
        //   } 
        //   else if ($transaction == 'deny') {
        //   // TODO set payment status in merchant's database to 'Denied'
        //   echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        // }
    }

    public function getStatus(Request $request){
        $allData = $request->only('order_id');
        $order = DB::table('payment_transaction')->where('order_id',$allData['order_id'])->first();
        if (count($order)==1) {
            $duration_since_order = time() - $order->transaction_time;
            //check If the order after 360 seconds (6 minutes), and still no pending/capture/settlement/deny/cancel/failure status.
            if ( (time()-strtotime($order->transaction_time)) <= 6  && $order->transaction_status === null ){
                //Do get status for that order
                return '{
                          "status_code": "500",
                          "status_message": "Request stills under 6 minutes, manual check can do after 6 minutes request does not update"
                        }';
            }else{
                $objResult = Midtrans::status($order->order_id);
                $result = array();
                foreach ($objResult as $key => $value) {
                    $result[$key] = $value;
                }
                print_r($result);
                
            }
            
        }elseif (count($order)>1) {
            return '{
                          "status_code": "500",
                          "status_message": "Double order id"
                        }';
        }else{
            return '{
                      "status_code": "404",
                      "status_message": "The requested resource is not found"
                    }';
        }
        
        /**
         * Assume we can load order data from database to $orders array
         * Each $order is an object, with property of:
         * $order->timestamp : unix timestamp when order is created (timestamp)
         * $order->order_id  : order_id that is sent to Midtrans API (string)
         * $order->transaction_status : status of the order (string)
         * $order->set_status($status): update order status according to parameter (string)
         * $order->save()    : save the order to database
         */
    }

    public function finishTransaction(Request $request)
    {
        $getReq = $request->only('order_id','status_code');

        
        $checkdata = DB::table('payment_transaction')
        				->where('order_id',$getReq['order_id'])
        				->first();

        if (count($checkdata)==0) {
            DB::table('payment_transaction')->insert($getReq);
        }else{
            DB::table('payment_transaction')
            	->where('order_id',$getReq['order_id'])
            	->update($getReq);    
        }

        $getOrder = DB::table('tbl_order')
        				->where('order_id',$getReq['order_id'])
        				->first();

        $getVoucher = DB::table('voucher')
        				->where('voucher_id',$getOrder->voucher_id)
        				->first();
        
        if (count($getVoucher)==0) {
            $res = array(
            	'responeCode'=>1,
            	'responeMessage'=>"Voucher Iten not found",
            	'status'=>"Failed"
            );
            return response()->json($res,404);
        }
        $checkInvoice = DB::table('invoice')
        					->where('order_id',$getReq['order_id'])
        					->first();

        $total_order  = $getOrder->total;
        $voucher_add = 0;
        if ($getVoucher->count_buy!=0) {
            $total_order =$getOrder->total+floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
            $voucher_add = floor($getOrder->total/$getVoucher->count_buy)*$getVoucher->get_buy;
        }
        if (count($checkInvoice)==0) {
            $invoice_number = HlmHelper::genInvoiceNumber($getOrder->customer_id);
            $saveToInvoice = array(
            	'voucher_id'=>$getVoucher->voucher_id,
				'invoice_number'=>$invoice_number,
				'company_id' =>$getVoucher->company_id,
				'merchant_id'=>$getVoucher->merchant_id,
				'customer_id'=>$getOrder->customer_id,
				'order_id'=>$getOrder->order_id,
				'order_date'=>date('Y-m-d H:i:s'),
				'total'=>$total_order,
				'bonus_voucher'=>$getVoucher->get_buy,
				'total_price'=>$getOrder->total*$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
				'payment_type'=>$getVoucher->payment_type,
				'create_date'=>date("Y-m-d H:i:s"),
				'change_date'=>date("Y-m-d H:i:s"),
				'change_by'=>$getOrder->change_by
			);
            DB::table('invoice')->insert($saveToInvoice);

            for ($i=0; $i <$getOrder->total; $i++) { 
                $saveToInvoiceHistory = array(
                	'order_id' =>$getOrder->order_id,
					'name'=>$getVoucher->name,
					'invoice_number'=>$invoice_number,
					'voucher_id'=>$getVoucher->voucher_id,
					'price'=>$getVoucher->price * ((100-$getVoucher->discount)/100) * ((100-$getVoucher->discount_plus)/100),
					'point'=>$getVoucher->point,
					'bonus_point'=>$getVoucher->bonus_point,
					'payment_type'=>$getVoucher->payment_type,
					'voucher_category'=>$getVoucher->voucher_category,
					'expired_date'=>$getVoucher->expired_date,
					'create_date'=>date('Y-m-d H:i:s'),
					'change_date'=>date('Y-m-d H:i:s'),
					'change_by'=>$getOrder->change_by
				);
                DB::table('invoice_history')->insert($saveToInvoiceHistory);
            }

            if ($getOrder->total_order>=$getVoucher->count_buy && $getVoucher->count_buy!=0 && $getVoucher->get_buy!=0) {
                for ($i=0; $i < $voucher_add; $i++) { 
                    $saveToInvoiceHistory = array(
						'order_id' =>$getOrder->order_id,
						'name'=>$getVoucher->name,
						'invoice_number'=>$invoice_number,
						'voucher_id'=>$getVoucher->voucher_id,
						'price'=>0,
						'point'=>0,
						'bonus_point'=>$getVoucher->bonus_point,
						'payment_type'=>$getVoucher->payment_type,
						'voucher_category'=>$getVoucher->voucher_category,
						'expired_date'=>$getVoucher->expired_date,
						'voucher_type'=>1,
						'create_date'=>date('Y-m-d H:i:s'),
						'change_date'=>date('Y-m-d H:i:s'),
						'change_by'=>$getOrder->change_by
					);
                    DB::table('invoice_history')->insert($saveToInvoiceHistory);
                }
            }
        }


        echo "Selamat Transaksi Telah Selesai";
    }
}    