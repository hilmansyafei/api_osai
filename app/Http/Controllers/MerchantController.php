<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use App\Http\Requests;
use Auth;
use Hash;
use QRcode;
use App\Helper\HlmHelper;
use App\Mail\MailOsai;
use Mail;

class MerchantController extends Controller
{
    //API MERCHANT
    public function changePasswordMerchant(Request $request)
    {
        $getReq = $request->only('old_password','new_password');
        $user = JWTAuth::parseToken()->authenticate();
        $credentials = array('username'=>$user['username'],
                             'password'=>$getReq['old_password']);
        $code = 200;
        if (!Auth::attempt($credentials)) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Old password is invalid",
                         'status'=>"failed");
            $code = 404;
        }else{
            DB::table('users')
            ->where('id',$user['id'])
            ->where('user_group',13)
            ->update(array('password'=>Hash::make($getReq['new_password'])));

            $res = array('responeCode'=>0,
                         'responeMessage'=>"Change password success",
                         'status'=>"Success");
        }
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function profileMerchant(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','users.merchant_id','=','merchant.id')
                        ->leftJoin('invoice as i','i.merchant_id','=','users.merchant_id')
                        ->LeftJoin('invoice_history as ih','i.order_id','=','ih.order_id')
                        ->select('users.id',
                                 'users.name',
                                 'users.email', 
                                 'users.username',
                                 'users.foto',
                                 'company.name as company_name',
                                 'merchant.name as merchant_name',
                                 'merchant.balance',
                                 'merchant.latit',
                                 'merchant.longit',
                                 'merchant.address',
                            DB::raw('(select sum(if(withdraw_status = 0,ih.price,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id and ih.`qr_code` is not null and used = 1) as available_withdraw'),
                            DB::raw('(select sum(if(withdraw_status = 0,1,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id and ih.`qr_code` is not null and used = 1) as v_available_withdraw'),
                            DB::raw('(select sum(if(withdraw_status = 1,ih.price,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id) as process_balance'),
                            DB::raw('(select sum(if(withdraw_status = 1,1,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id) as v_process_balance'),
                            DB::raw('(select sum(if(withdraw_status = 2,ih.price,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id) as transferred_balance'),
                            DB::raw('(select sum(if(withdraw_status = 2,1,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id) as v_transferred_balance'),
                            DB::raw('(select sum(if(withdraw_status = 3,ih.price,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id) as canceled'),
                            DB::raw('(select sum(if(withdraw_status = 3,1,0)) 
                                    from invoice_history as ih 
                                        join invoice as i 
                                            on ih.order_id = i.order_id
                                        where i.merchant_id = users.merchant_id) as v_canceled'))
                        ->where('users.id',$user['id'])->first();
        $res = array('profile'=>$profile,
                     'responeCode'=>0,
                     'responeMessage'=>'Load Success',
                     'status'=>'Success');
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function listLastActivity(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $lim = 3;
        $offset = 0;
        if ($request->has('limit')) {
            $input = $request->only('limit');
            $lim = $input['limit'];
        }

        if ($request->has('offset')) {
            $input = $request->only('offset');
            $offset = $input['offset'];
        }

        $profile = DB::table('invoice_history')
                        ->leftJoin('withdraw_history','invoice_history.id','=','withdraw_history.id_invoice_history')
                        ->join('invoice','invoice.invoice_number','=','invoice_history.invoice_number')
                        ->join('users','invoice_history.cashier_id','=','users.id')
                        ->where('invoice.company_id',$user['company_id'])
                        ->where('invoice.merchant_id',$user['merchant_id'])
                        ->orderBy('invoice_history.id','desc')
                        ->select('users.name','invoice_history.voucher_id','invoice_history.price','invoice_history.point','invoice_history.payment_type','invoice_history.create_date','invoice_history.id as id_history','users.id as id_cashier',DB::raw('if(withdraw_history.id is null,"1","2") as type'),DB::raw('if(withdraw_history.id is null,"9",if(withdraw_history.status=1,"Requesting",if(withdraw_history.status=2,"Approved","Rejected"))) as status'))
                        ->offset($offset)
                        ->take($lim)
                        ->get();

        $res = array('activities'=>$profile,
                     'responeCode'=>0,
                     'responeMessage'=>'Load Success',
                     'status'=>'Success');
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function getListRedeem(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $lim = 0;
        $offset = 0;
        $from_date = "2017-01-01";
        $to_date = (date('Y')+1)."-01-01";
        $type_date = 1;//1 for redeem 2 for withdraw
        $q = "";
        $stat_withdraw = 0;
        $operateWithdraw = "=";

        if (!$request->has('limit') || !$request->has('offset')) {
            $res = array(
                        'responeCode'=>1,
                        'responeMessage'=>'Limit or offset parameter not found',
                        'status'=>'Failed'
                    );
            return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
        }

        if ($request->has('from_date') && $request->has('to_date')) {
            $input = $request->only('from_date','to_date','type_date');
            $from_date = $input['from_date'];
            $to_date = $input['to_date'];
            $type_date = $input['type_date'];

            if (!$request->has('type_date')) {
                $res = array(
                        'responeCode'=>1,
                        'responeMessage'=>'Type date parameter not found',
                        'status'=>'Failed'
                    );
                return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
            }
        }

        if ($request->has('type_date')) {
            $input = $request->only('type_date');
            $type_date = $input['type_date'];
        }

        if ($request->has('keyword')) {
            $input = $request->only('keyword');
            $q = $input['keyword'];
        }

        if ($request->has('stat_withdraw')) {
            $input = $request->only('stat_withdraw');
            $stat_withdraw = $input['stat_withdraw'];
            if ($stat_withdraw == 4) {
                $operateWithdraw = "!=";
                $stat_withdraw = 0;
            }
        }else{
            $stat_withdraw = "5";
            $operateWithdraw = "!=";
        }

        if ($type_date == 1) {
            $type_q = "invoice_history.redeem_date";
        }else{
            $type_q = "withdraw_history.request_date";
        }

        $input = $request->only('limit','offset');
        $lim = $input['limit'];
        $offset = $input['offset'];

        $dataRedems = DB::table('invoice_history')
                        ->leftJoin('withdraw_history','invoice_history.id','=','withdraw_history.id_invoice_history')
                        ->join('invoice','invoice.invoice_number','=','invoice_history.invoice_number')
                        ->join('users','invoice_history.cashier_id','=','users.id')
                        ->join('users as customer','invoice.customer_id','=','customer.id')
                        ->join('voucher as v','v.voucher_id','=','invoice_history.voucher_id')
                        ->where('invoice.company_id',$user['company_id'])
                        ->where('invoice.merchant_id',$user['merchant_id'])
                        ->where('invoice_history.withdraw_status',$operateWithdraw,$stat_withdraw)
                        ->whereBetween($type_q,[$from_date,$to_date])
                        ->whereNotNull('invoice_history.redeem_date')
                        ->where(function ($query) use ($q) {
                                $query->where('v.name', 'like', "%$q%")
                                      ->orWhere('v.voucher_id', 'like', "%$q%");
                            })
                        ->orderBy('invoice_history.id','desc')
                        ->select('invoice_history.order_id',
                                 'invoice_history.voucher_id',
                                 'v.name as voucher_name',
                                 'invoice_history.price',
                                 'invoice_history.redeem_date',
                                 'withdraw_history.request_date as withdraw_date',
                                 'invoice_history.id as id_history',
                                 'users.id as id_cashier',
                                 'users.name as cashier_name',
                                 'customer.id as customer_id',
                                 'customer.name as customer_name',
                                 DB::raw('if(withdraw_status = 1,"Requesting",
                                            if(withdraw_status = 2, "Approved",
                                                if(withdraw_status = 0,"Available","Canceled")
                                            )
                                          ) as withdraw_status')
                                )
                        ->offset($offset)
                        ->limit($lim)
                        ->get();

        $fixData = array();
        foreach ($dataRedems as $key => $value) {
            $data = array(
                'cashier'=>array(
                            'id'=>$value->id_cashier,
                            'name'=>$value->cashier_name,
                           ),
                'customer'=>array(
                             'customer_id'=>$value->customer_id,
                             'customer_name'=>$value->customer_name,
                            ),
                'order_id'=>$value->order_id,
                'voucher_id'=>$value->voucher_id,
                'voucher_name'=>$value->voucher_name,
                'price'=>$value->price,
                'redeem_date'=>$value->redeem_date,
                'withdraw_date'=>$value->withdraw_date,
                'id_history'=>$value->id_history,
                'withdraw_status'=>$value->withdraw_status
            );
            array_push($fixData, $data);
        }

        $res = array('redeems'=>$fixData,
                     'responeCode'=>0,
                     'responeMessage'=>'Load Success',
                     'status'=>'Success');
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function getListWithdraw(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $listWithdraw = DB::table('invoice_history')
                        ->join('withdraw_history','invoice_history.id','=','withdraw_history.id_invoice_history')
                        ->join('invoice','invoice.invoice_number','=','invoice_history.invoice_number')
                        ->join('users','invoice_history.cashier_id','=','users.id')
                        ->where('invoice.company_id',$user['company_id'])
                        ->where('invoice.merchant_id',$user['merchant_id'])
                        ->where(DB::raw('date(now())'),'>=',DB::raw('date_add(invoice.order_date , INTERVAL 1 DAY)'))
                        ->orderBy('invoice_history.id','desc')
                        ->select('users.name','invoice_history.voucher_id','invoice_history.price','invoice_history.point','invoice_history.payment_type','invoice_history.create_date',DB::raw('withdraw_history.request_date as date'),'invoice_history.id as id_history','users.id as id_cashier',DB::raw('if(withdraw_history.id is null,"9",if(withdraw_history.status=1,"Requesting",if(withdraw_history.status=2,"Approved","Rejected")) ) as status',DB::raw('"2" as type')))
                        ->get();

        $res = array('activities'=>$listWithdraw,
                     'responeCode'=>0,
                     'responeMessage'=>'Load Success',
                     'status'=>'Success');
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function getRedeem(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $input = $request->only('from','to');

        $profile = DB::table('invoice_history')
                        ->join('invoice','invoice.invoice_number','=','invoice_history.invoice_number')
                        ->join('users','invoice_history.cashier_id','=','users.id')
                        ->where('invoice.company_id',$user['company_id'])
                        ->where('invoice.merchant_id',$user['merchant_id'])
                        ->where('invoice_history.withdraw_status',0)
                        ->whereBetween(DB::raw('DATE_FORMAT(invoice_history.redeem_date,"%Y-%m-%d")'), [$input['from'], $input['to']])
                        ->orderBy('invoice_history.id','desc')
                        ->select('users.name','invoice_history.price','invoice_history.create_date','invoice_history.id as id_history','users.id as id_cashier')
                        ->get();

        $res = array('activities'=>$profile,
                     'responeCode'=>0,
                     'responeMessage'=>'Load Success',
                     'status'=>'Success');
        return response()->json($res)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function requestWithdraw(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $input = $request->only('list_id_history','bank_account_id');
        $listIdHistory = $input['list_id_history'];

        $listIdHistory = (json_decode($listIdHistory));
        $listIdHistory =  implode(",",$listIdHistory);
        $checkWithdraw = DB::select("select * from 
                                        invoice_history 
                                            where id in ($listIdHistory) and withdraw_status = 0");
        $code = 200;
        if (count($checkWithdraw)==0) {
            $res = array('responeCode'=>1,
                     'responeMessage'=>'Voucher has been withdrawed',
                     'status'=>'Failed');
            $code = 404;
        }else{
            foreach ($checkWithdraw as $key => $value) {
                $dataInsert = array('id_invoice_history'=>$value->id,
                                    'manager_id'=>$user['id'],
                                    'status'=>1,
                                    'request_date'=>date('Y-m-d H:i:s'),
                                    'bank_account_id'=>$input['bank_account_id']);
                HlmHelper::updateData('invoice_history',array('withdraw_status'=>1),'id='.$value->id);
                HlmHelper::insertData('withdraw_history',$dataInsert);
            }

            $res = array('responeCode'=>0,
                         'responeMessage'=>'Load Success',
                         'status'=>'Success');
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
    //END API MERCHANT
}
