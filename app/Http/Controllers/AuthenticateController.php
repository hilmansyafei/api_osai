<?php

 

namespace App\Http\Controllers;

 

namespace App\Http\Controllers;

 

use App\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use JWTAuth;

use JWTFactory;

use DB;

use Tymon\JWTAuth\Exceptions\JWTException;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;

use Tymon\JWTAuth\Exceptions\TokenInvalidException;

use Auth;

 

class AuthenticateController extends Controller {

    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index() {

    }

    public function hashPass($pass='')
    {
        //DB::table('users')->update(array('password'=>Hash::make('hilman')));
        return Hash::make($pass);
    }

    //AUTH APP USER
    public function authApp($username="",$password="")
    {
        $credentialsEmail = array('email'=>$username,'password'=>$password);
        $credentialsUsername = array('username'=>$username,'password'=>$password);

        if (Auth::attempt($credentialsEmail)) {
            return 1;
        }else{
            if (Auth::attempt($credentialsUsername)) {
                return 1;
            }else{
                return 0;
            }
        }
    }

    public function authenticate(Request $request) 
    {
        $getReq = $request->only('username', 'password','fcm_token');
        $credentials = array('username' =>$getReq['username'] ,'password'=>$getReq['password']);
        $user = $credentials['username'];
        $pass = $credentials['password'];

        $checkUser = DB::table('users')
                        ->where('username',$user)
                        ->where('user_group',0)
                        ->first();

        if (count($checkUser) == 0) {
            $checkUser = DB::table('users')
                        ->where('email',$user)
                        ->where('user_group',0)
                        ->first();
            if (count($checkUser)==0) {
                $resp = array('error' => 'invalid_credentials',
                              'responseMessage' => 'Username and password do not match');
                return response()->json($resp, 404);
            }else{
                $user = $credentials['username'] = $checkUser->username;
            }
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                //if (!$token = JWTAuth::attempt($credentials, $customClaims)) {
                $resp = array('error' => 'invalid_credentials',
                              'responseMessage' => 'Username and password do not match');
                return response()->json($resp, 404);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        //update FCM token
        DB::table('users')->where('username',$user)->update(array('fcm_token'=>$getReq['fcm_token']));
        // if no errors are encountered we can return a JWT
        $token = compact('token');

        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','merchant.merchant_id','=','merchant.id')
                        ->select('users.*', 'company.name as company_name','merchant.name as merchant_name')
                        ->where('users.id',$checkUser->id)->first();
        unset($profile->password);
        unset($profile->company_id);
        unset($profile->merchant_id);

        return response()->json(array(
            "token"=>$token['token'],"profile"=>$profile,
            'responseCode'=>0,
            'responseMessage'=>"Load Success",
            'status'=>"Success")
        );
    }
    
    public function authenticateFacebook(Request $request) 
    {
        $getReq = $request->only('facebook_id','fcm_token');
         //$getReq['facebook_id'] = '{"id":"10203889993646211","email":"imanbellamy@gmail.com","first_name":"Hilman","last_name":"Syafei","gender":"male"}';
        $dataLogin  = json_decode($getReq['facebook_id']);
        $getUser = DB::table('users')->where('facebook_id',$dataLogin->id)->first();
        //echo "<pre>";print_r($getUser);die();
        if (count($getUser)==0) {
            //return response()->json(['error' => 'invalid_credentials'], 401);
            $checkEmail = DB::table('users')->where('email',$dataLogin->email)->first();
            if (count($checkEmail)>0) {
                $getUser = DB::table('users')->where('email',$dataLogin->email)->first();
                DB::table('users')->where('email',$dataLogin->email)->update(array('facebook_id'=>$dataLogin->id));
            }else{
                $dataRegis = array(
                    'email'=>$dataLogin->email,
                    'name'=>$dataLogin->first_name." ".$dataLogin->last_name,
                    'facebook_id'=>$dataLogin->id,
                    'digital_user_id'=>date('YmdHis')
                );
                DB::table('users')->insert($dataRegis);
                $getUser = DB::table('users')->where('facebook_id',$dataLogin->id)->first();
            }
        }

        $credentials = array('username'=>$getUser->email,'sub'=>$getUser->id);
        $payload = JWTFactory::make($credentials);
        $token = JWTAuth::encode($payload)->get();

        DB::table('users')
            ->where('id',$getUser->id)
            ->update(array('fcm_token'=>$getReq['fcm_token']));

        // if no errors are encountered we can return a JWT
        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','merchant.merchant_id','=','merchant.id')
                        ->select('users.*', 'company.name as company_name','merchant.name as merchant_name')
                        ->where('facebook_id',$dataLogin->id)
                        ->first();

        unset($profile->password);
        unset($profile->company_id);
        unset($profile->merchant_id);

        $res = array(
            "token"=>$token,
            "profile"=>$profile,
            'responseCode'=>0,
            'responseMessage'=>"Login Success",
            'status'=>"Success"
        );
        return response()->json($res);
    }

    public function authenticateGoogle(Request $request) 
    {
        $getReq = $request->only('google_id','fcm_token');
        // $getReq['google_id'] = '{"id":"113856713382336295967","email":"fitrah.ramadhan26@gmail.com","displayName":"Fitrah Ramadhan","givenName":"Fitrah","familyName":"Ramadhan","photoUrl":"https:\/\/lh6.googleusercontent.com\/-_8-wM8zvt78\/AAAAAAAAAAI\/AAAAAAAABP0\/9suAWOv9sVw\/photo.jpg"}';
        $dataLogin  = json_decode($getReq['google_id']);
        $getUser = DB::table('users')
                       ->where('google_id',$dataLogin->id)
                       ->first();

        if (count($getUser)==0) {
            //return response()->json(['error' => 'invalid_credentials'], 401);
            $checkEmail = DB::table('users')->where('email',$dataLogin->email)->first();
            if (count($checkEmail)>0) {
                $getUser = DB::table('users')->where('email',$dataLogin->email)->first();
            }else{
                $dataRegis = array(
                    'email'=>$dataLogin->email,
                    'name'=>$dataLogin->displayName,
                    'google_id'=>$dataLogin->id,
                    'digital_user_id'=>date('YmdHis')
                );
                //'username'=>strtolower($dataLogin->givenName),
                DB::table('users')->insert($dataRegis);
                $getUser = DB::table('users')
                               ->where('google_id',$dataLogin->id)
                               ->first();
            }
        }

        $credentials = array('username'=>$getUser->email,'sub'=>$getUser->id);

        DB::table('users')
            ->where('id',$getUser->id)
            ->update(array('fcm_token'=>$getReq['fcm_token']));

        $payload = JWTFactory::make($credentials);
        $token = JWTAuth::encode($payload)->get();
        $code = 200;
        // if no errors are encountered we can return a JWT
        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','merchant.merchant_id','=','merchant.id')
                        ->select('users.*', 'company.name as company_name','merchant.name as merchant_name')
                        ->where('users.email',$dataLogin->email)->first();

        if (count($profile)==0) {
            $res = array(
                'token'=>$token,
                'profile'=>$profile,
                'responseCode'=>1,
                'responseMessage'=>"Login Failed",
                'status'=>"Failed"
            );
            $code = 404;
        }else{
            unset($profile->password);
            unset($profile->company_id);
            unset($profile->merchant_id);
            $res = array(
                'token'=>$token,
                'profile'=>$profile,
                'responseCode'=>0,
                'responseMessage'=>"Login Success",
                'status'=>"Success"
            );
        }
        return response()->json($res,$code);
    }
    
    public function getAuthenticatedUser() 
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function register(Request $request) 
    {
        ##email, username, name, password, phone_number, address, facebook_id, google_id
        $newuser = $request->all();
        // if (isset($newuser['first_name'])) {
        //     $newuser['name'] = $newuser['first_name'];
        //     unset($newuser['first_name']);
        // }
        $email = $request->input('email');
        $username = $request->input('username');
        $password = Hash::make($request->input('password'));
        $checkAvailableEmail = DB::table('users')->where('email',$email)->count();
        $checkAvailableUsername = DB::table('users')->where('username',$username)->count();
        $newuser['level_user'] = 4;
        $newuser['create_date'] =date("Y-m-d H:i:s");
        $newuser['change_date'] =date("Y-m-d H:i:s");
        $newuser['digital_user_id'] =date("YmdHis");

        if ($checkAvailableUsername) {
            return response()->json(array(
                'responseCode'=>1,
                'responseMessage'=>"Username is not available",
                'status'=>"Failed"),
                404
            );
        }elseif($checkAvailableEmail){
            return response()->json(array(
                'responseCode'=>1,
                'responseMessage'=>"Email is not available",
                'status'=>"Failed"),
                404
            );
        }
        $newuser['password'] = $password;

        if (DB::table('users')->insert($newuser)) {
            return response()->json(array(
                'responseCode'=>0,
                'responseMessage'=>"Register Success",
                'status'=>"Success")
            );
        }else{
            return response()->json(array(
                'responseCode'=>1,
                'responseMessage'=>"Register Failed",
                'status'=>"Failed"),
                404
            );
        }
    }
    //END AUTH APP USER

    //AUTH APP CASHIER
    public function authenticateCashier(Request $request) 
    {
        $getReq = $request->only('username', 'password');
        $credentials = array('email' =>$getReq['username'] ,'password'=>$getReq['password']);
        $user = $credentials['email'];
        $pass = $credentials['password'];   

        $checkUser = DB::table('users')
                        ->where('email',$user)
                        ->where('user_group',11)
                        ->first();
        if (count($checkUser)==0) {

            $resp = array('error' => 'invalid_credentials',
                              'responseMessage' => 'Username and password do not match');
                return response()->json($resp, 404);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $resp = array('error' => 'invalid_credentials',
                              'responseMessage' => 'Username and password do not match');
                return response()->json($resp, 404);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        $token = compact('token');
        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','merchant.merchant_id','=','merchant.id')
                        ->select('users.*', 'company.name as company_name','merchant.name as merchant_name')
                        ->where('users.email',$user)
                        ->where('user_group',11)
                        ->first();
        unset($profile->password);
        unset($profile->company_id);
        unset($profile->merchant_id);
        return response()->json(array(
            "token"=>$token['token'],
            "profile"=>$profile,
            'responseCode'=>0,
            'responseMessage'=>"Load Success",
            'status'=>"Success")
        );
    }
    //END AUTH APP CASHIER

    //API MERCHANT
    public function authenticateMerchant(Request $request) 
    {
        $getReq = $request->only('username', 'password');
        $credentials = array('username' =>$getReq['username'] ,'password'=>$getReq['password']);
        $user = $credentials['username'];
        $pass = $credentials['password'];   

        $checkUser = DB::table('users')
                        ->where('email',$user)
                        ->where('user_group',13)
                        ->first();
        if (count($checkUser)==0) {
            $resp = array('responseMessage' => 'Username and password do not match',
                          'responseCode' => 1);
                return response()->json($resp, 400);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $resp = array('responseMessage' => 'Username and password do not match',
                              'responseCode' => 1);
                return response()->json($resp, 400);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        $getDataUser = DB::table('users')
                    ->where('users.email',$user)
                    ->where('user_group',13)
                    ->first();
        if (empty($getDataUser->merchant_id)) {
           return response()->json(array(
                'token'=>$token['token'],
                'profile'=>$profile,
                'responseCode'=>1,
                'responseMessage'=>"User is not valid, there is problem when creating this user",
                'status'=>"Failed")
            ); 
        }  
        $token = compact('token');
        $profile = DB::table('users')
                        ->leftJoin('company','company.company_id','=','users.company_id')
                        ->leftJoin('merchant','merchant.merchant_id','=','merchant.id')
                        ->select('users.*', 'company.name as company_name','merchant.name as merchant_name', DB::raw('(select sum(price) 
                                from invoice_history as ih 
                                    join invoice as i 
                                        on ih.order_id = i.order_id
                                    where i.merchant_id = '.$getDataUser->merchant_id.' and withdraw_status not in (2,3)) as saldo_money '),
                        DB::raw('(select sum(point) 
                                from invoice_history as ih 
                                    join invoice as i 
                                        on ih.order_id = i.order_id
                                    where i.merchant_id = '.$getDataUser->merchant_id.'
                                    and withdraw_status not in (2,3)) as saldo_point'))
                        ->where('users.email',$user)
                        ->where('user_group',13)
                        ->first();

        unset($profile->password);
        unset($profile->company_id);
        unset($profile->merchant_id);
        return response()->json(array(
            'token'=>$token['token'],
            'profile'=>$profile,
            'responseCode'=>0,
            'responseMessage'=>"Load Success",
            'status'=>"Success")
        );
    }
    //END API MERCHANT
}