<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use App\Http\Requests;
use Auth;
use Hash;
use QRcode;
use App\Helper\HlmHelper;
use App\Mail\MailOsai;
use Mail;

class CashierController extends Controller
{
    //API CASHIER
    public function changePasswordCashier(Request $request)
    {
        $getReq = $request->only('old_password','new_password');
        $user = JWTAuth::parseToken()->authenticate();
        $credentials = array('username'=>$user['username'],
                             'password'=>$getReq['old_password']);
        $code = 200;
        if (!Auth::attempt($credentials)) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Old password is invalid",
                         'status'=>"failed");
            $code = 404;
        }else{
            DB::table('users')
            ->where('id',$user['id'])
            ->where('user_group',11)
            ->update(array('password'=>Hash::make($getReq['new_password'])));

            $res = array('responeCode'=>0,
                     'responeMessage'=>"Change password success",
                     'status'=>"Success");
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function verifyVoucher(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('qr_code');
        $getVoucher = DB::table('invoice_history')
            ->join('voucher','voucher.voucher_id','=','invoice_history.voucher_id')
            ->join('invoice','invoice.order_id','=','invoice_history.order_id')
            ->join('users','users.id','=','invoice.customer_id')
            ->join('merchant','invoice.merchant_id','=','merchant.id')
            ->where('qr_code',$getReq['qr_code'])
            ->select('voucher.*',
                     'invoice_history.qr_code',
                     'invoice_history.used',
                     'users.name as name_user',
                     'users.email',
                     'users.digital_user_id',
                     'merchant.name as merchant_name',
                     'merchant.email as merchant_email',
                     'merchant.phone_number')
            ->first();

        $code = 200;
        if (count($getVoucher)==0) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Voucher Not Found",
                         'status'=>"Failed");
            $code = 404;
        }elseif($user['company_id']!=$getVoucher->company_id){
            $res = array('responeCode'=>1,
                         'responeMessage'=>"You cannot scan voucher from another company",
                         'status'=>"Failed");
            $code = 404;
        }elseif ($user['merchant_id']!=$getVoucher->merchant_id) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"You cannot scan voucher from another merchant",
                         'status'=>"Failed");
            $code = 404;
        }elseif ($getVoucher->used==1) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Voucher Has Beed Used",
                         'status'=>"Failed");
            $code = 404;
        }elseif (strtotime($getVoucher->expired_date)<strtotime(date('Y-m-d H:i:s'))) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Voucher Has Beed Expired",
                         'status'=>"Failed");
            $code = 404;
        }else{
            $res = array('voucher'=>$getVoucher,'responeCode'=>0,
                     'responeMessage'=>"Load Success",
                     'status'=>"Success");
        }
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function redeemVoucher(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $getReq = $request->only('qr_code');
        $code = 200;
        $getVoucher = DB::table('invoice_history')
            ->join('voucher','voucher.voucher_id','=','invoice_history.voucher_id')
            ->join('invoice','invoice.order_id','=','invoice_history.order_id')
            ->join('users','invoice.customer_id','=','users.id')
            ->where('qr_code',$getReq['qr_code'])
            ->select('voucher.*','invoice_history.qr_code','invoice_history.voucher_id','invoice_history.used','users.fcm_token')
            ->first();

        //print_r($getVoucher);die();
        if (count($getVoucher)==0) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Voucher Not Found",
                         'status'=>"Failed");
            $code = 404;
        }elseif($user['company_id']!=$getVoucher->company_id){
            $res = array('responeCode'=>1,
                         'responeMessage'=>"You cannot scan voucher from another company",
                         'status'=>"Failed");
            $code = 404;
        }elseif ($user['merchant_id']!=$getVoucher->merchant_id) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"You cannot scan voucher from another merchant",
                         'status'=>"Failed");
            $code = 404;
        }elseif ($getVoucher->used==1) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Voucher Has Beed Used",
                         'status'=>"Failed");
            $code = 404;
        }elseif (strtotime($getVoucher->expired_date)<strtotime(date('Y-m-d H:i:s'))) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Voucher Has Beed Expired",
                         'status'=>"Failed");
            $code = 404;
        }else{
            $confirmation_number = HlmHelper::genVerNumber();
            $resNotif = HlmHelper::sendNotif($getVoucher->fcm_token,'['.$confirmation_number.'] Notif ini karena anda redeem voucher '.$getVoucher->name.' Voucher ID : '.$getVoucher->voucher_id);

            $arrNotif = json_decode($resNotif);
            
            if($arrNotif->success==0){
                $res = array('responeCode'=>1,
                             'responeMessage'=>"Fcm not registered",
                             'status'=>"Failed");
                $code = 404;
            }else{
                DB::table('invoice_history')
                    ->where('qr_code',$getReq['qr_code'])
                    ->update(array('confirmation_number'=>$confirmation_number,'cashier_id'=>$user['id']));
                $retNotif = json_decode($resNotif);

                $res = array('responeCode'=>0,
                             'responeMessage'=>"Load Success",
                             'status'=>"Success");
            }
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function confirmationUser(Request $request){
        $getReq = $request->only('confirmation_number','qr_code');
        $getVoucher = DB::table('invoice_history')
            ->join('voucher','voucher.voucher_id','=','invoice_history.voucher_id')
            ->join('invoice','invoice.order_id','=','invoice_history.order_id')
            ->join('users','invoice.customer_id','=','users.id')
            ->where('confirmation_number',$getReq['confirmation_number'])
            ->where('qr_code',$getReq['qr_code'])
            ->select('voucher.*','invoice_history.qr_code','invoice_history.used','users.fcm_token')
            ->first();
        $code = 200;
        if (count($getVoucher)==0) {
            $res = array('responeCode'=>1,
                         'responeMessage'=>"Conformation number is not valid",
                         'status'=>"Failed");
            $code = 404;
        }else{
            $resNotif = HlmHelper::sendNotif($getVoucher->fcm_token,"Voucher (".$getVoucher->qr_code.") berhasil digunakan","Informasi Voucher");
            DB::table('invoice_history')
                ->where('qr_code',$getReq['qr_code'])
                ->update(array('redeem_date'=>date('Y-m-d H:i:s'),'used'=>1));
            
            $res = array('responeCode'=>0,
                     'responeMessage'=>"Redeem Success",
                     'status'=>"Success");
        }
        
        return response()->json($res,$code)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
    //END API CASHIER
}
